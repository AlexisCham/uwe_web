<?php
require("bdd_connect.php");
class Mark
{
    
	private $id;
	private $userId;
	private $componentId;
	private $mark;

	public function __construct($id,$userId,$componentId,$mark)
	{
		$this-> id = $id;
		$this-> userId = $userId;
		$this-> componentId = $componentId;
		$this-> mark = $mark;
	}

	public function __get($attribut){
		$ret = null;
        switch($attribut){
			case 'id': {$ret = $this->id;break;}
			case 'userId': {$ret = $this->userId;break;}
            case 'componentId': {$ret = $this->componentId;break;}
            case 'mark': {$ret = $this->mark;break;}
            default: {$ret = null;}
        }
        return $ret;
	}

	static function getAllMarks(){
		require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $q = "SELECT id,userId,componentId,mark FROM marks;";
            $response = $bdd->query($q);

            $marks = array();
            while($line = $response->fetch()){
                array_push($marks, new Mark(
                                            $line['id'],
                                            $line['userId'],
                                            $line['componentId'],
                                            $line['mark']
                                            ));
            }

            $response->closeCursor();
            return $marks;

        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    static function getMark($markId){
        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT id, userId, componentsId, mark FROM marks WHERE marks.id=?;");
            $response->execute(array($markId));
            $rep = $response->fetch();
            $response->closeCursor();
            
            return new Mark(
                            $rep['id'],
                            $rep['userId'],
                            $rep['componentId'],
                            $rep['mark']
                            );


        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    static function getMarkDetails($markId){
        // TODO: Cette fonction ne sert pas a grand chose pour le moment,
        // et ne fournit pas vraiment plus de details.
        // A voir quels champs seront necessaires lors de l'ajout des tableaux
        // dans le HTML (nom prenom eleves ou pas ... ?)

        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

        
            $response = $bdd->prepare("SELECT marks.id as markId, mark, components.id as componentId, users.id as userId
                                        FROM `components`, users, marks 
                                        WHERE components.id = ? 
                                        AND marks.componentId=components.id 
                                        AND marks.userId=users.id");
            $response->execute(array($markId));
            $mark = array();
            $line = $response->fetch();
            $response->closeCursor();
            $mark['id'] = $line['markid'];
            $mark['mark'] = $line['mark'];
            $mark['componentId'] = $line['componentId'];
            $mark['userId'] = $line['userId'];

            return $mark;

        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    static function addMark($mark, $studentId, $componentId){
        require("bdd_connect.php");
        try{
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare("INSERT INTO marks VALUES ('',?,?,?) ;");
            $reponse->execute(array($mark, $studentId, $componentId));
            $reponse->closeCursor();
        }catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
        }
    }

    static function delMark($markId){
        require("bdd_connect.php");
        try{
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare("DELETE FROM marks WHERE id=?;");
            $reponse->execute(array($componentId));
            $reponse->closeCursor();
        }catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
        }
    }
}
?>
