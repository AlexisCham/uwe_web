<?php
require("component.class.php");

class Module{

    private $id;
    private $name;
    private $details;

    public function __construct($id, $name, $details){
        $this->id = $id;
        $this->name = $name;
        $this->details = $details;
    }

    public function __get($attribut){
        $ret = null;
        switch($attribut){
            case 'id': {$ret = $this->id;break;}
            case 'name': {$ret = $this->name;break;}
            case 'details': {$ret = $this->details;break;}
            default: {$ret = null;}
        }
        return $ret;
    }

    static function getAllModules(){
        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $q = "SELECT id,name,details FROM modules;";
            $response = $bdd->query($q);

            $modules = array();
            while($line = $response->fetch()){
                array_push($modules, new Module(
                                                $line['id'],
                                                $line['name'],
                                                $line['details']
                                                ));
            }

            $response->closeCursor();
            return $modules;

        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    static function getModuleById($moduleId){
        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT id, name, details FROM modules WHERE modules.id=?;");
            $response->execute(array($moduleId));
            $rep = $response->fetch();
            $response->closeCursor();
            
            return new Module($rep['id'],
                              $rep['name'],
                              $rep['details']
                              );


        }
        catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    static function getModuleComponents($moduleId){
        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT * FROM components WHERE components.moduleId=$moduleId");
            $response->execute(array($moduleId));

            $components = array();
            while($line = $response->fetch()){
                array_push($components, new Component(
                                                $line['id'],
                                                $line['coefficient'],
                                                $line['examDate'],
                                                $line['isResit'],
                                                $line['moduleId'],
                                                $line['typeId']
                                            ));
            }
            return $components;
        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }

    }

    static function addModule($name, $details){
        require("bdd_connect.php");
        try{
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare("INSERT INTO modules VALUES ('',?,?) ;");
            $reponse->execute(array($name, $details));
            $reponse->closeCursor();
        }catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
        }
    }

    static function delModule($moduleId){
        require("bdd_connect.php");
        try{
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare("DELETE FROM modules WHERE id=?;");
            $reponse->execute(array($moduleId));
            $reponse->closeCursor();
        }catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
        }
    }

    static function getModuleGradeDecisionTable($userId){
        // Renvoie les details du module demande
        // format : pour la page exam

        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT modules.name, modules_results.grade, modules_results.resit 
                                        FROM modules,modules_results 
                                        WHERE modules_results.moduleId = modules.id
                                        AND modules_results.userId=?"
                                        );
            $response->execute(array($userId));

            $moduleTable = array();
            while($line = $response->fetch()){
                array_push($moduleTable, array(
                                                $line['name'],
                                                $line['grade'],
                                                $line['resit']
                                            ));
            }

            return $moduleTable;


        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }

    }

    static function getModuleGradeDecisionTableAdmin(){
        // Renvoie les details du module demande
        // format : pour la page exam

        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->query("SELECT modules.name, modules_results.grade, modules_results.resit, modules_results.userId
                                        FROM modules,modules_results 
                                        WHERE modules_results.moduleId = modules.id"
                                        );

            $moduleTable = array();
            while($line = $response->fetch()){
                array_push($moduleTable, array(
                                                $line['name'],
                                                $line['grade'],
                                                $line['resit'],
                                                User::getUser($line['userId'])[0]->name." ".User::getUser($line['userId'])[0]->surname
                                            ));
            }

            return $moduleTable;


        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }

    }

//////
    static function getStudentModulesEnrolments($userId){
        // Renvoie les modules dans lequel est inscrit letudiant donne
        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT enrolments.moduleId FROM enrolments WHERE enrolments.userId = ?;");
            $response->execute(array($userId));


            $modulesId = array();
            while($line = $response->fetch()){
                array_push($modulesId, $line['moduleId']);
            }
            return $modulesId;


        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    static function getAllModulesId(){
        // Renvoie les modules dans lequel est inscrit letudiant donne
        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT id FROM modules;");
            $response->execute(array());


            $modulesId = array();
            while($line = $response->fetch()){
                array_push($modulesId, $line['id']);
            }
            return $modulesId;


        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    static function getStudentAllModulesResultsTable($userId){
        // Renvoie la table de la page exam
        $yop = Module::getModuleGradeDecisionTable($userId);
        $table = array();
        foreach ($yop as $key => $value) {
            array_push($table,$value);
        }
        return $table;

    }
/////// 
    static function getStudentAllModulesResultsTableAdmin(){
        // Renvoie la table de la page exam
        $yop = Module::getModuleGradeDecisionTableAdmin();
        $table = array();
        foreach ($yop as $key => $value) {
            array_push($table,$value);
        }
        return $table;

    }

    static function getModulesComponentsWeights($moduleId){
        // renvoie un tableau avec le nb de components et le nom des components
        // OK
        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT
                                            COUNT(components.id) as c,
                                            components_types.type
                                        FROM
                                            components, components_types
                                        WHERE
                                            components.typeId=components_types.id
                                        AND
                                            components.moduleId=?
                                        GROUP BY components_types.type");

            $response->execute(array($moduleId));

            $weights = array();
            while($line = $response->fetch()){
                

                array_push($weights, array($line['c'], $line['type']));
            }
            return $weights;


        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }

    }

    static function getUserMarksModule($userId, $moduleId){

        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT components_types.type, components.coefficient, marks.mark 
                                        FROM components, components_types, marks
                                        WHERE 
                                        components.typeId=components_types.id
                                        AND marks.componentId=components.id
                                        AND components.moduleId=?
                                        AND marks.userId=?");

            $response->execute(array($moduleId, $userId));

            $marks = array();
            while($line = $response->fetch()){
                array_push($marks, array($line['type'],
                                         $line['coefficient'],
                                         $line['mark']
                                        ));
            }

            return ($marks);
        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }


    }

    static function getMarksModule($moduleId){

        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT users.name, users.surname, components_types.type, components.coefficient, marks.mark 
                                        FROM components, components_types, marks, users
                                        WHERE 
                                        components.typeId=components_types.id
                                        AND marks.componentId=components.id
                                        AND components.moduleId=?
                                        AND marks.userId=users.id
                                        ORDER BY users.name");

            $response->execute(array($moduleId));

            $marks = array();
            while($line = $response->fetch()){
                array_push($marks, array(
                                         $line['type'],
                                         $line['coefficient'],
                                         $line['mark'],
                                         $line['name'],
                                         $line['surname']
                                        ));
            }

            return ($marks);
        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }


    }

    static function getAllModulesData($userId){
        // OK
        $dt = array(); // 
        $moduleIncrm = 1;
        // Pour chaque module dans lequel est enrol le student
        foreach (Module::getStudentModulesEnrolments($userId) as $key => $value) {
            $thisModule = array();
            $module = Module::getModuleById($value);
            $components = Module::getModuleComponents($value);

            $weights_compsNames = Module::getModulesComponentsWeights($value);
            $marks = Module::getUserMarksModule($userId, $value);
            
            array_push($thisModule,
                       "Module ".$moduleIncrm,
                       $module->name,
                       $module->details,
                       Module::getMean($marks),
                       $weights_compsNames,
                       $marks
                       );

            array_push($dt, $thisModule);  // Ajout du module
            $moduleIncrm += 1;
        }
        return $dt;
    }

    static function getMean($marks){
        //input : table 
        /*
        [
            ['Assignment', 30, 85],
            ['Lab Tests', 20, 70],
            ['Written Exam', 50, 90]
        ]
        */
        $note = 0;
        foreach ($marks as $line) {
            $note += ($line[1]/10)*$line[2];
        }
        return $note;

    }

    static function tableForMarkAdministration(){
        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT components.typeId FROM components WHERE components.moduleId=$moduleId");
            $response->execute(array($moduleId));

            $components = array();
            while($line = $response->fetch()){
                array_push($components, new Component(
                                                $line['id'],
                                                $line['coefficient'],
                                                $line['examDate'],
                                                $line['isResit'],
                                                $line['moduleId'],
                                                $line['typeId']
                                            ));
            }
            return $components;
        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    static function getAllModulesDataAllStudents(){
        // TODO: enlever les usersId

        $dt = array(); // 
        $moduleIncrm = 1;
        // Pour chaque module
        foreach (Module::getAllModulesId() as $key => $value) {
            $thisModule = array();
            $module = Module::getModuleById($value);
            $components = Module::getModuleComponents($value);

            $weights_compsNames = Module::getModulesComponentsWeights($value);
            $marks = Module::getMarksModule($value);
            


            array_push($thisModule,
                       "Module ".$moduleIncrm,
                       $module->name,
                       $module->details,
                       Module::getMean($marks),
                       $weights_compsNames,
                       $marks
                       );

            array_push($dt, $thisModule);  // Ajout du module
            $moduleIncrm += 1;
        }
        return $dt;
    }

}

?>