<?php


class Component{
    private $id;
    private $coefficient;
    private $examDate;
    private $isResit;
    private $moduleId;
    private $typeId;

    public function __construct($id, $coefficient, $examDate, $isResit, $moduleId, $typeId){

        $this->id = $id;
        $this->coefficient = $coefficient;
        $this->examDate = $examDate;
        $this->isResit = $isResit;
        $this->moduleId = $moduleId;
        $this->typeId = $typeId;
    }

    public function __get($attribut){
        $ret = null;
        switch($attribut){
            case 'id': {$ret = $this->id;break;}
            case 'coefficient': {$ret = $this->coefficient;break;}
            case 'examDate': {$ret = $this->examDate;break;}
            case 'isResit': {$ret = $this->isResit;break;}
            case 'moduleId': {$ret = $this->moduleId;break;}
            case 'typeId': {$ret = $this->typeId;break;}
            default: {$ret = null;}
        }
        return $ret;
    }

    static function getAllComponents(){
        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $q = "SELECT id,coefficient,examDate,isResit, moduleId,typeId FROM components;";
            $response = $bdd->query($q);

            $components = array();
            while($line = $response->fetch()){
                array_push($components, new Component(
                                                $line['id'],
                                                $line['coefficient'],
                                                $line['examDate'],
                                                $line['isResit'],
                                                $line['moduleId'],
                                                $line['typeId']
                                                ));
            }

            $response->closeCursor();
            return $components;

        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    static function getAllComponentsTypes(){
        require("bdd_connect.php");
        try{
            
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");
                    $response = $bdd->query("SELECT id,type FROM components_types ;");

            $componentTypeTable=array();
            //$componentTypeTable[0] = array();

            while ($ligne= $response->fetch()){
               /* array_push($componentTypeTable["idType"],$ligne['id']);
                array_push($componentTypeTable[0]["nameType"],$ligne['type']);*/
                $oneType = array("idType"=>$ligne['id'],"nameType"=>$ligne['type']);
                $componentTypeTable[] = $oneType;
            }
            
            $response->closeCursor();
            return $componentTypeTable;

        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    
    static function getComponentDetails($componentId){
        require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT components.id,coefficient,examDate,isResit,modules.name,components_types.type 
            FROM `components`,modules,components_types 
            WHERE modules.id=components.moduleId AND components_types.id=components.typeId AND components.id=?");

            $response->execute(array($componentId));

            $component = array();
            $line = $response->fetch();
            $response->closeCursor();
            
            $component['id'] = $line['id'];
            $component['coefficient'] = $line['coefficient'];
            $component['examDate'] = $line['examDate'];
            $component['isResit'] = $line['isResit'];
            $component['moduleName'] = $line['name'];
            $component['componentType'] = $line['type'];


            return $component;

        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    static function addComponent($coefficient, $examDate, $isResit, $moduleId, $typeId){
        require("bdd_connect.php");
        try{
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare("INSERT INTO components VALUES ('',?,?,?,?,?) ;");
            $reponse->execute(array($coefficient, $examDate, $isResit, $moduleId, $typeId));
            $reponse->closeCursor();
        }catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
        }
    }

    static function delComponent($componentId){
        require("bdd_connect.php");
        try{
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare("DELETE FROM components WHERE id=?;");
            $reponse->execute(array($componentId));
            $reponse->closeCursor();
        }catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
        }
    }

}

?>