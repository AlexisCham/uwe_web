<?php
class User
{
    private $id ; 
	private $name ;
	private $surname ;
	private $email ;
	private $address ; 
	private $password ;
	private $userTypeId;
	
	
	public function __construct($id,$name,$surname,$email,$address,$password,$userTypeId)
	{
		$this-> id= $id;
		$this-> name= $name;
		$this-> surname = $surname;
		$this-> email = $email ;
		$this-> address = $address ;
		$this-> password= $password;
		$this-> userTypeId = $userTypeId;
		
	}

	public function __get($attribut){
		$ret = null;

		switch ($attribut) {
			case 'id': {$ret = $this->id;break;}
			case 'name':{$ret = $this->name;break;}
			case 'surname':{$ret = $this->surname;break;}
			case 'email':{$ret = $this->email;break;}
			case 'address':{$ret = $this->address;break;}
			case 'password': {$ret = $this->password;break;}
			case 'userTypeId': {$ret = $this->userTypeId;break;}
			default: {$ret = null;}
		}
		return $ret;
	}


    static function getAllUsers() {
		require("bdd_connect.php");
		try{            
            
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
			$bdd->exec("set character set UTF8");
	                $response = $bdd->query("SELECT id,name,surname,email,address,password,userTypeId FROM users ;");

			$usersTable=array();
			while ($ligne= $response->fetch()){
	            array_push($usersTable, new User(
							$ligne['id'],
							$ligne['name'],
							$ligne['surname'],
							$ligne['email'],
							$ligne['address'],
							$ligne['password'],
							$ligne['userTypeId']
							   ) 
						);
	        }
			
			$response->closeCursor();
			return $usersTable;

	 	}catch (Exception $e){
	            die('Erreur : ' . $e->getMessage());
		}
	}


   static public function addUser($name,$surname,$email,$address,$pass,$userTypeId,$userName)
   {
   	require("bdd_connect.php");
		try
	{
                    
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);


                $reponse = $bdd->prepare("INSERT INTO users VALUES ('',?,?,?,?,?,?,?) ;");
                $reponse->execute(array($name,$surname,$email,$address,$pass,$userTypeId,$userName));
		$reponse->closeCursor();  // ferme le curseur
                
		
		}
		
	 catch (Exception $e)
	{
        die('Erreur : ' . $e->getMessage());
	}
	
   }


   static function getUser($id) {
		require("bdd_connect.php");
		try
		{            
            
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
			$bdd->exec("set character set UTF8");
	                $response = $bdd->prepare("SELECT id,name,surname,email,address,password,userTypeId FROM users WHERE id = ?;");
					$response->execute(array($id));
			$user=array();
			while ($ligne= $response->fetch())  
			{
	                   array_push($user, new User(
	                   	$ligne['id'],
	                   	$ligne['name'],
	                   	$ligne['surname'],
	                   	$ligne['email'],
	                   	$ligne['address'],
	                   	$ligne['password'],
	                   	$ligne['userTypeId']
	                   ) );

	                     
	        }
			
			$response->closeCursor();  // ferme le curseur
				
			return $user; // On retourne le tableau des revues
	 	}
		 catch (Exception $e)
		{
	            die('Erreur : ' . $e->getMessage());
		}
	
   }

   static public function modifyUser($name,$firstname,$email,$address,$id)
   {
   	require("bdd_connect.php");
		try
	{
                    
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
                
                       
                $response = $bdd->prepare("UPDATE users SET name=?, surname=?, email=?, address=? WHERE id=? ;");
                $response->execute(array($name,$firstname,$email,$address,$id));
		$response->closeCursor();  // ferme le curseur
                
		
		}
		
	 catch (Exception $e)
	{
        die('Erreur : ' . $e->getMessage());
	}
	
   }


   	static public function delUser($num){
   		require("bdd_connect.php");
		try{
                    
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
                
                     
                $reponse = $bdd->prepare("DELETE FROM users Where id = ? ;");
                $reponse->execute(array($num));
		$reponse->closeCursor();
                
		
		}catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
		}

	}

	/*
	static function vider()
	{
		//TODO: Trouver une utilite a ca
                unserialize($_SESSION['panier']);
                unset($_SESSION['panier']);
                
	}
	*/
	
	static public function verifyLogInfo($userName, $hash){
		// non teste
		require("bdd_connect.php");
        try{
            header('Content-Type: text/html; charset=utf-8');
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT id, userName FROM users WHERE userName=? AND password=?;");
            $response->execute(array($userName, $hash));
            $rep = $response->fetch();  // une seule ligne
            
			
			if($rep['userName'] == $userName){
				return $rep['id'];
			}
			return 0;
			$response->closeCursor();			

        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
	}

	static function getRole($userId){
		require("bdd_connect.php");
        try{
        	
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		    $bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
            $bdd->exec("set character set UTF8");

            $response = $bdd->prepare("SELECT userTypeId FROM users WHERE users.id=?;");
            $response->execute(array($userId));
            $rep = $response->fetch();  // une seule ligne
            $response->closeCursor();
			
			if(isset($rep['userTypeId']) && ($rep['userTypeId'] != null)){
				return $rep['userTypeId'];
			}
			return 0;

        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
	}

	static function getUserType(){
		require("bdd_connect.php");
        try{
        	
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
			$bdd->exec("set character set UTF8");
	                $response = $bdd->query("SELECT id,type FROM users_types ;");

			$userTypeTable=array();
			//$userTypeTable[0] = array();

			while ($ligne= $response->fetch()){
	           /* array_push($userTypeTable["idType"],$ligne['id']);
	            array_push($userTypeTable[0]["nameType"],$ligne['type']);*/
	            $oneType = array("idType"=>$ligne['id'],"nameType"=>$ligne['type']);
	            $userTypeTable[] = $oneType;
	        }
			
			$response->closeCursor();
			return $userTypeTable;

        }catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
	}

	static public function addUserToModule($userId,$moduleId)
   {
   	require("bdd_connect.php");
		try
	{
                    
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);


                $reponse = $bdd->prepare("INSERT INTO enrolments VALUES ('',?,?) ;");
                $reponse->execute(array($userId,$moduleId));
		$reponse->closeCursor();  // ferme le curseur
                
		
		}
		
	 catch (Exception $e)
	{
        die('Erreur : ' . $e->getMessage());
	}
	
   }

   static public function delUserToModule($userId,$moduleId){
   		require("bdd_connect.php");
		try{
                    
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$bdd = new PDO('mysql:host='.$host.';dbname='.$bdd, $util, $password, $pdo_options);
                
                     
                $reponse = $bdd->prepare("DELETE FROM users Where id = ? ;");
                $reponse->execute(array($num));
		$reponse->closeCursor();
                
		
		}catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
		}

	}

}



?>
