<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title>UWE Marks ZBEG</title>
    <link rel="stylesheet" href="View/Include/Css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="View/Include/Css/style.css">
    <link rel="icon" type="image/png" href="View/Include/img/uwe-logo.png"/>
</head>
<header>
    <?php
    if (isset($_GET['page']) && $_GET['page'] == 'login')
    {
        $page=$_GET["page"];
    }
    else if(!isset($_GET['page']))
    {
      $page="home";
      include "View/Include/header.inc.php";
    }
    else
    {
      $page=$_GET["page"];
      include "View/Include/header.inc.php";
    }
    ?>
</header>
<body>
<?php

if(!isset($_GET['page']))
{
      include "View/Include/index.inc.php"; // si $page n'est pas d�finie, alors la page d'accueil se lance
}

else
{
       // si la page est d�finie, alors, on l'inclut !
	  $page=$_GET["page"];
      if(file_exists("Controller/$page.php"))	 // on v�rifie que le nom du fichier contenu dans la variable $page existe
      {
      include "Controller/$page.php";
      }

     else
    {

    include "View/Include/error.inc.php"; // La page demand�e est introuvable, le serveur renvoie 404

     }
}
?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="View/Include/Css/bootstrap/js/bootstrap.min.js"></script>
<?php
if (isset($_GET['page']) && $_GET['page'] == 'login')
{
    $page=$_GET["page"];
}
else
{
include "View/Include/footer.inc.php";
}
?>

</body>
</html>
