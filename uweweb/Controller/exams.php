<?php 
if(!empty($_SESSION['connexion']))
{	
	$role = User::getRole($_SESSION['connexion']);
	if ($role == 1) {
		include('Model/module.class.php');
		$modulesTable = Module::getStudentAllModulesResultsTable($_SESSION['connexion']);
		include("View/exams.php");
	}
	else if ($role == 3) {
		include('Model/module.class.php');
		$modulesTable = Module::getStudentAllModulesResultsTableAdmin();
		include("View/exams.php");

	}
	else{
		echo "
		<br>
		<h1>
		&nbsp Error : You do not have acces to this page !
		</h1>
		";
	}
	
	
}
else
{
	echo "
	<br>
	<h1>
	&nbsp Error : You have to be connected to access to this page !
	</h1>
	<h2>
	&nbsp Please use the login button on the top right of the screen.
	</h2>
	";
}
?>