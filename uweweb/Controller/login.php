<?php 
session_start();

require("Model/user.class.php");


if(!(isset($_POST['username']) && isset($_POST['password']))){
	// Premiere arrivee sur la page
}else{
	if (!(empty($_POST['username']) && empty($_POST['password']))){
		// Les champs username et password remplis
		// Vérification des identifiants
		$testUserId = User::verifyLogInfo($_POST['username'],$_POST['password']);
		if($testUserId != 0){
			// Connexion reussie
			$_SESSION['connexion'] = $testUserId;
			header("Location:index.php");
		}else{
			// Connexion echouee
			echo "<script type=\"text/javascript\"> alert('Connection failed')</script>";
		}
	}else{
		echo "<script type=\"text/javascript\"> alert('Please fill both username and password fields ')</script>";
		// Les champs username et password ne sont pas remplis
	}
}


include("View/login.php");
?>
