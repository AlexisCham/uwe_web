<?php 
if(!empty($_SESSION['connexion']))
{	
	$role = User::getRole($_SESSION['connexion']);
	if ($role == 3) {
		
		
		$userTypeTable = User::getUserType();

		$allUserTable = User::getAllUsers();

		include("Model/module.class.php");
		$allModuleTable = Module::getAllModules();


		$componentTypeTable = Component::getAllComponentsTypes();

		include("View/administration.php");
	}
	else{
		echo "
		<br>
		<h1>
		&nbsp Error : You do not have acces to this page !
		</h1>
		";
	}
}

else 
{
	echo "
	<br>
	<h1>
	&nbsp Error : You have to be connected to access to this page !
	</h1>
	<h2>
	&nbsp Please use the login button on the top right of the screen.
	</h2>
	";
}

?>