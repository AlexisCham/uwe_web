<?php 
if(!empty($_SESSION['connexion']))
{
	include("Model/module.class.php");

	//var_dump(User::getRole($_SESSION['connexion']));

	if(User::getRole($_SESSION['connexion']) == "1"){
		$dt = Module::getAllModulesData($_SESSION['connexion']);
	}else{
		$dt = Module::getAllModulesDataAllStudents();
	}
	
	
	//print_r($dt);
	include("View/modules.php");
}

else 
{
	echo "
	<br>
	<h1>
	&nbsp Error : You have to be connected to access to this page !
	</h1>
	<h2>
	&nbsp Please use the login button on the top right of the screen.
	</h2>
	";}

?>