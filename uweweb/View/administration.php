<link rel="stylesheet" href="View/Include/Css/administration.css">
<link rel="stylesheet" href="View/Include/Css/profile.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

<section >

<div>
  <div id="accordion">
  <br/>
  <h2>Administration</h2>
  <p>Use this tab to manage <strong>users</strong>, <strong>students enrolment</strong>, <strong>modules</strong>, <strong>exams</strong> and <strong>marks</strong>.</p>
  <br/>
    <div class="card">
      <div class="card-header">
        <button class="card-link aco-link" data-toggle="collapse" href="#collapseOne">
          Users
        </button>
      </div>
      <div id="collapseOne" class="collapse" data-parent="#accordion">
        <div class="card-body">
        <h3>Add User</h3>
            <form name="myform" method='POST' action='index.php?page=user_manage'>

             
              <div class="form-group">
                <label for="sel1">User Type</label>
                <select class="form-control" name="userTypeId" id="sel1" value="userTypeId">
                  <option> - Select user types</option>
                  <?php 
                    for ($i=0; $i <count($userTypeTable) ; $i++) { 
                    ?>  
                        <option name="userTypeId" value='<?php echo $userTypeTable[$i]["idType"] ?>'> <?php echo $userTypeTable[$i]["nameType"]?></option>
                    <?php
                    }
                  ?>
                </select>
              </div>
               

              <br/>

            <!---Row1-->

                <div class="form-row">

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 1-->

                    <div class="form-group col-md-4">
                        <label>First Name</label>
                        <input type="text" name="surname" class="form-control" id="param1">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 2-->

                    <div class="form-group col-md-1">
                    </div>

                    <div class="form-group col-md-4">
                        <label>Last Name</label>
                        <input type="text" name="name" class="form-control" id="param2">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

                </div>

            <!---Row2-->

                <div class="form-row">

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 3-->
                    <div class="form-group col-md-4">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" id="param3">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 4-->

                    <div class="form-group col-md-1">
                    </div>

                    <div class="form-group col-md-4">
                        <label>Address</label>
                        <input type="text" name="address" class="form-control" id="param4">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

                </div>

            <!---Row3-->

                <div class="form-row">

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 5-->

                    <div class="form-group col-md-4">
                        <label>Password</label>
                        <input type="password" name="clearPassword" class="form-control" id="clearpassfield1">
                        <input id="hashpassfield1" type="hidden" name="password_hashed" value="">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

                </div>
                <input type='hidden' name='action' value='1'/>
                <input type="submit" class="btn btn-primary" id="add" value="Add">
                 

            </form>
            <script src="View/Include/js/forge.min.js"></script>

            <script type="text/javascript">

              document.getElementById('clearpassfield1').addEventListener('input', function (evt) {
               document.getElementById('hashpassfield1').value = forge_sha256(document.getElementById('clearpassfield1').value);
              });

            </script>

          <br/>
          <h3>Modify/Delete User</h3>

            <form action="/html/tags/html_form_tag_action.cfm">
              <fieldset class="form-group">
                <label>Choose a User</label>
                <select class="form-control" id="sel1">
                  <option> - Select a user</option>
                  <?php 
                    for($index=0;$index < count($allUserTable);$index++) {
                        $oneuser = $allUserTable[$index];
                    ?>  
                        <option name="userid" value='<?php echo $oneuser->id ?>'> <?php echo $oneuser->name." ".$oneuser->surname;?></option>
                    <?php
                    }
                  ?>
                </select>
              </fieldset>

              <div class="form-group">
                <label for="sel1">User Type</label>
                <select class="form-control" id="sel1">
                  <option></option>
                  <option>Student</option>
                  <option>Teacher</option>
                  <option>Administator</option>
                </select>
              </div>

              <br/>

            <!---Row1-->

                <div class="form-row">

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 1-->

                    <div class="form-group col-md-4">
                        <label>First Name</label>
                        <input type="text" class="form-control" id="param1">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 2-->

                    <div class="form-group col-md-1">
                    </div>

                    <div class="form-group col-md-4">
                        <label>Last Name</label>
                        <input type="text" class="form-control" id="param2">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

                </div>

            <!---Row2-->

                <div class="form-row">

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 3-->
                    <div class="form-group col-md-4">
                        <label>Email</label>
                        <input type="email" class="form-control" id="param3">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 4-->

                    <div class="form-group col-md-1">
                    </div>

                    <div class="form-group col-md-4">
                        <label>Address</label>
                        <input type="text" class="form-control" id="param4">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

                </div>

            <!---Row3-->

                <div class="form-row">

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 5-->

                    <div class="form-group col-md-4">
                        <label>Password</label>
                        <input type="password" class="form-control" id="param5">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

                </div>

              <div class="form-row">

                <div class="col-md-1">
                </div>

                <div class="col-md-2">
                  <input type="submit" class="btn btn-primary" id="modify" value="Modify">
                </div>

                <div class="col-md-5">
                </div>

                <div class="col-md-2">
                  <input type="submit" class="btn btn-primary" id="delete" value="Delete">
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>


    <div class="card">
      <div class="card-header">
        <button class="collapsed card-link aco-link" data-toggle="collapse" href="#collapseTwo">
          Students Enrolment
        </button>
      </div>
      <div id="collapseTwo" class="collapse" data-parent="#accordion">
        <div class="card-body">
        <h3>Add a Student to a Module</h3>
          <form name="myform2" method='POST' action='index.php?page=user_manage'>
            <fieldset class="form-group">
              <label>Choose a Student</label>
              <select class="form-control" name="userId" id="sel1" value="userId">
              <option> - select a user</option>
             <?php 
                    for($index=0;$index < count($allUserTable);$index++) {
                        $oneuser = $allUserTable[$index];
                    ?>  
                        <option name="userId" value='<?php echo $oneuser->id ?>'> <?php echo $oneuser->name." ".$oneuser->surname; ?></option>
                    <?php
                    }
                  ?>
            </select>
            </fieldset>
            <fieldset class="form-group">
              <label>Choose a Module</label>
              <select class="form-control" name="moduleId" id="sel1" value="moduleId">
                  <option> - select a module</option>
                  <?php 
                    for($index=0;$index < count($allModuleTable);$index++) {
                        $oneModule = $allModuleTable[$index];
                    ?>  
                        <option name="moduleId" value='<?php echo $oneModule->id ?>'> <?php echo $oneModule->name; ?> </option>
                    <?php
                    }
                  ?>
                </select>         
            </fieldset>
            <input type='hidden' name='action' value='4'/>
            <input type="submit" class="btn btn-primary" id="add" value="Add">
          </form>

        <br/>
        <h3>Delete a Student from a Module</h3>

          <form action="/html/tags/html_form_tag_action.cfm">
            <fieldset class="form-group">
              <label>Choose a Student</label>
              <select class="form-control" name="userId" id="sel1" value="userId" >
              <option> - select a user</option>
             <?php 
                    for($index=0;$index < count($allUserTable);$index++) {
                        $oneuser = $allUserTable[$index];
                    ?>  
                        <option name="userId" value='<?php echo $oneuser->id ?>'> <?php echo $oneuser->name." ".$oneuser->surname; ?></option>
                    <?php
                    }
                  ?>
            </select>
            </fieldset>
            <fieldset class="form-group">
              <label>Choose a Module</label>
              <select class="form-control" name="moduleId" id="sel1" value="moduleId">
                   <option> - select a module</option>
                  <?php 
                    for($index=0;$index < count($allModuleTable);$index++) {
                        $oneModule = $allModuleTable[$index];
                    ?>  
                        <option name="moduleId" value='<?php echo $oneModule->id ?>'> <?php echo $oneModule->name; ?> </option>
                    <?php
                    }
                  ?>
                </select>
            </fieldset>
            <input type='hidden' name='action' value='5'/>
            <input type="submit" class="btn btn-primary" id="delete" value="Delete">
          </form>

        </div>
      </div>
    </div>


    <div class="card">
      <div class="card-header">
        <button class="collapsed card-link aco-link" data-toggle="collapse" href="#collapseThree">
          Modules
        </button>
      </div>
      <div id="collapseThree" class="collapse" data-parent="#accordion">
        <div class="card-body">
        <h3>Add Module</h3>
          <form name="myform4" method='POST' action='index.php?page=administration_manage'>
            <fieldset class="form-group">
              <label>Name</label>
              <input type="text" class="form-control" id="module_name" name="module_name">
            </fieldset>
            <fieldset class="form-group">
              <label>Details</label>
              <input type="text" class="form-control" id="module_detail" name="module_detail">
            </fieldset>
            <input type='hidden' name='action' value='1'/>
            <input type="submit" class="btn btn-primary" id="add" value="Add">
          </form>
        <br/>
        <h3>Modify/Delete Module</h3>
          <form action="/html/tags/html_form_tag_action.cfm">
            <fieldset class="form-group">
              <label>Choose a Module</label>
              <select class="form-control" id="sel1">
                <option></option>
                <option>Web Programming (WP)</option>
                <option>Web Design (WD)</option>
                <option>Content Management System (CMS)</option>
                <option>Legal Ethical Social and Professional Issues (LESPI)</option>
                <option>Web Development Frameworks (WDF)</option>
                <option>Web Technologies (WT)</option>
              </select>
            </fieldset>
            <fieldset class="form-group">
              <label>Name</label>
              <input type="text" class="form-control" id="first_name" name="first_name">
            </fieldset>
            <fieldset class="form-group">
              <label>Details</label>
              <input type="text" class="form-control" id="last_name" name="last_name">
            </fieldset>
            <div class="form-row">

              <div class="col-md-1">
              </div>

              <div class="col-md-2">
                <input type="submit" class="btn btn-primary" id="modify" value="Modify">
              </div>

              <div class="col-md-5">
              </div>

              <div class="col-md-2">
                <input type="submit" class="btn btn-primary" id="delete" value="Delete">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>


    <div class="card">
      <div class="card-header">
        <button class="collapsed card-link aco-link" data-toggle="collapse" href="#collapseFour">
          Exams
        </button>
      </div>
      <div id="collapseFour" class="collapse" data-parent="#accordion">
        <div class="card-body">
        <h3>Add Exam</h3>
          <form name="myform5" method='POST' action='index.php?page=administration_manage'>
              <div class="form-group">
                <label for="sel1">Module</label>
                <select class="form-control" name="moduleId" id="sel3" value="moduleId" >
                  <option> - select a module </option>
                  <?php 
                    for($index=0;$index < count($allModuleTable);$index++) {
                        $oneModule = $allModuleTable[$index];
                    ?>  
                        <option name="moduleId" value='<?php echo $oneModule->id ?>'> <?php echo $oneModule->name; ?> </option>
                    <?php
                    }
                  ?>
                </select>
              </div>

              <br/>

            <!---Row1-->

                <div class="form-row">

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 1-->

                    <div class="form-group col-md-4">
                      <label>Type of Exam</label>
                      <select class="form-control" name="componentTypeId" id="sel3" value="componentTypeId" >
                        <option></option>
                        <?php 
                        for ($i=0; $i <count($componentTypeTable) ; $i++) { 
                        ?>  
                            <option name="componentTypeId" value='<?php echo $componentTypeTable[$i]["idType"] ?>'> <?php echo $componentTypeTable[$i]["nameType"]?></option>
                        <?php
                        }
                      ?>
                      </select>
                    </div>

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 2-->

                    <div class="form-group col-md-1">
                    </div>

                    <div class="form-group col-md-4">
                        <label>Coefficient (0-100)</label>
                        <input type="text" name="coefficient" class="form-control" id="param2">
                    </div>

                    <div class="form-group col-md-1">
                    </div>

                </div>

            <!---Row2-->

                <div class="form-row">

                    <div class="form-group col-md-1">
                    </div>

            <!---Param 3-->
                    <div class="form-group col-md-4">
                    <label>Date</label>
                      <div class="form-group">
                        <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                          <input type="text" name="date" class="form-control datetimepicker-input" data-target="#datetimepicker1"s/>
                          <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                        </div>
                      </div>
                      <script type="text/javascript">
                        $(function () {
                          $('#datetimepicker1').datetimepicker();
                        });
                      </script>
                    </div>

                    <div class="form-group col-md-1">
                    </div>

                </div>
                <input type='hidden' name='action' value='2'/>
                <input type="submit" class="btn btn-primary" id="add" value="Add">

            </form>

          <br/>
          <h3>Modify/Delete Exam</h3>

            <form action="/html/tags/html_form_tag_action.cfm">
              <fieldset class="form-group">
              <label>Choose a Module</label>
              <select class="form-control" id="sel1">
                  <option></option>
                  <option>Web Programming (WP)</option>
                  <option>Web Design (WD)</option>
                  <option>Content Management System (CMS)</option>
                  <option>Legal Ethical Social and Professional Issues (LESPI)</option>
                  <option>Web Development Frameworks (WDF)</option>
                  <option>Web Technologies (WT)</option>
                </select>
              </fieldset>
              <fieldset class="form-group">
                <label>Choose an Exam</label>
                <select class="form-control" id="sel1">
                    <option></option>
                    <option>Assignment 1</option>
                    <option>Assignment 2</option>
                    <option>Lab Tests 1</option>
                    <option>Lab Tests 2</option>
                    <option>Written Exam 1</option>
                    <option>Written Exam 2</option>
                </select>
              </fieldset>


              <!---Row1-->

              <div class="form-row">

                <div class="form-group col-md-1">
                </div>

                <!---Param 1-->

                <div class="form-group col-md-4">
                  <label>Type of Exam</label>
                  <select class="form-control" id="sel1">
                    <option></option>
                    <option>Assignment</option>
                    <option>Lab Tests</option>
                    <option>Written Exam</option>
                  </select>
                </div>

                <div class="form-group col-md-1">
                </div>

                <!---Param 2-->

                <div class="form-group col-md-1">
                </div>

                <div class="form-group col-md-4">
                    <label>Coefficient (0-100)</label>
                    <input type="text" class="form-control" id="param2">
                </div>

                <div class="form-group col-md-1">
                </div>

                </div>

                <!---Row2-->

                <div class="form-row">

                <div class="form-group col-md-1">
                </div>

                <!---Param 3-->
                <div class="form-group col-md-4">
                <label>Date</label>
                  <div class="form-group">
                    <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2"/>
                      <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                  <script type="text/javascript">
                    $(function () {
                      $('#datetimepicker2').datetimepicker();
                    });
                  </script>
                </div>

                <div class="form-group col-md-1">
                </div>

              </div>


              <div class="form-row">

                <div class="col-md-1">
                </div>

                <div class="col-md-2">
                  <input type="submit" class="btn btn-primary" id="modify" value="Modify">
                </div>

                <div class="col-md-5">
                </div>

                <div class="col-md-2">
                  <input type="submit" class="btn btn-primary" id="delete" value="Delete">
                </div>
              </div>

            </form>


        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <button class="collapsed card-link aco-link" data-toggle="collapse" href="#collapseFive">
          Marks
        </button>
      </div>
      <div id="collapseFive" class="collapse" data-parent="#accordion">
        <div class="card-body">
        <h3>Assign/Delete Marks</h3>
        <form name="myform6" method='POST' action='index.php?page=administration_manage'>
          <div class="form-group">
            <label for="sel1">Student</label>
            <select class="form-control" name="userId" id="sel4">
              <option> - select a user </option>
              <?php 
                    for($index=0;$index < count($allUserTable);$index++) {
                        $oneuser = $allUserTable[$index];
                    ?>  
                        <option name="userId" value='<?php echo $oneuser->id ?>'> <?php echo $oneuser->name." ".$oneuser->surname; ?></option>
                    <?php
                    }
                  ?>
            </select>
          </div>

          <br/>

          <!---Row1-->

            <div class="form-row">

                <div class="form-group col-md-1">
                </div>

          <!---Param 1-->

                <div class="form-group col-md-4">
                  <label>Module</label>
                  <select class="form-control" name="moduleId" id="sel5">
                    <option> - select a module </option>
                  <?php 
                    for($index=0;$index < count($allModuleTable);$index++) {
                        $oneModule = $allModuleTable[$index];
                    ?>  
                        <option name="moduleId" value='<?php echo $oneModule->id ?>'> <?php echo $oneModule->name; ?> </option>
                    <?php
                    }
                  ?>
                  </select>
                </div>

                <div class="form-group col-md-1">
                </div>

          <!---Param 2-->

                <div class="form-group col-md-1">
                </div>

                <div class="form-group col-md-4">
                  <label>Component</label>
                  <select class="form-control" id="sel1">
                    <option></option>
                    <option>Assignment 1</option>
                    <option>Assignment 2</option>
                    <option>Lab Tests 1</option>
                    <option>Lab Tests 2</option>
                    <option>Written Exam 1</option>
                    <option>Written Exam 2</option>
                  </select>
                </div>

                <div class="form-group col-md-1">
                </div>

            </div>

          <!---Row2-->

            <div class="form-row">

              <div class="form-group col-md-7">
              </div>

            <!---Param 2-->

              <div class="form-group col-md-4">
                <label>Mark</label>
                <input type="text" class="form-control" id="param2">
              </div>

              <div class="form-group col-md-1">
              </div>

            </div>

            <div class="form-row">

              <div class="col-md-1">
              </div>

              <div class="col-md-2">
                <input type="submit" class="btn btn-primary" id="add" value="Add/Modify">
              </div>

              <div class="col-md-5">
              </div>

              <div class="col-md-2">
                <input type="submit" class="btn btn-primary" id="delete" value="Delete">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>



</section>
