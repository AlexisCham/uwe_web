<link rel="stylesheet" href="View/Include/Css/exams.css">
<script src="View/Include/js/sortTab.js"></script>

<div class="container">
  <br>

  <table id="tableExams" class="table table-bordered">
  <thead>
            <tr>    
                    <th onclick="sortTable(0)">Module <button type="button" name="module"></button></th>
                    <th onclick="sortTable(1)">Grade <button type="button" name="grade"></button></th>
                    <th onclick="sortTable(2)">Decision <button type="button" name="decision"></button></th>
                    <?php if (sizeof($modulesTable[0])>3) {
                      
                     ?>
                      <th onclick="sortTable(3)">User <button type="button" name="user"></button></th>
                    <?php
                         } 
                    ?>
            </tr>
    </thead>
    <tbody>
        <?php


        foreach ($modulesTable as $key => $value) {
                echo "<tr>";
                foreach ($value as $k => $v) {
                        echo "<td>";
                        echo $v;
                        echo "</td>";
                }
                echo "</tr>";
        }

        ?>
  
    
    </tbody>

  </table>
  <p> If you have to resit an exam please checkout payment here :  
  <a class="btn btn-primary" href="index.php?page=payment#" role="button">Payment</a>
<br>
</div>
