<link rel="stylesheet" href="View/Include/Css/login.css">

<div class="modal-dialog text-center">

  <div class="col-sm-8 main-section">

      <div class="modal-content">

          <div class="col-12 user-img">
          <a href="index.php">
            <img src="View/Include/img/uwe-logo.png" alt="logo UWE Marks ZBEG, return to home"/>
          <a>
          </div>
          <div class="col-12 form-input">
            <form name="myform" method='POST' action='index.php?page=login'>
              <div class="form-group">
                <input type="text" class="form-control" name="username" placeholder="Enter Username">
              </div>
              <div class="form-group">
                <input id="clearpassfield" type="password" class="form-control" name="clearpass" placeholder="Enter Password">
              </div>
              <input id="hashpassfield" type="hidden" name="password" value="">
              <button type="submit" class="success" name="btn btn-success">Login</button>
            </form>
          </div>

      </div>

  </div>

</div>

<script src="View/Include/js/forge.min.js"></script>

<script type="text/javascript">

    document.getElementById('clearpassfield').addEventListener('input', function (evt) {
        document.getElementById('hashpassfield').value = forge_sha256(document.getElementById('clearpassfield').value);
    });

</script>