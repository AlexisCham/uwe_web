<link rel="stylesheet" href="View/Include/Css/exams.css">
<link rel="stylesheet" href="View/Include/Css/modules.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="View/Include/js/sortTab.js"></script>

<ul class="nav nav-tabs" role="tablist">
  <?php  // Boucle pour chaque module
  foreach ($dt as $module) {
    $modName = str_replace(" ", "", $module[0]);
    echo "<li class=\"nav-item\">";
    echo "<a class=\"nav-link\" href=\"#$modName\" role=\"tab\" data-toggle=\"tab\">$modName</a>";
    echo "</li>";
  }
?>
</ul>




<!-- Tab panes -->


<div class="tab-content">


<!-- NOUVEAU -->

  <?php  // Boucle pour chaque module (dans le tab)
  $i = 0;
  foreach ($dt as $module) {
    $modNum = str_replace(" ", "", $module[0]);
    $modName = $module[1];
    $modDesc = $module[2];

    echo "<div role=\"tabpanel\" class=\"tab-pane\" id=\"$modNum\">";
      echo "<div class=\"container\">";

        // Titre desc
        echo "<div class=\"row justify-content-center\">";
          echo "<div class=\"present\">";
            echo "<h2 class=\"title\">$modName</h2>";
            echo "<p>$modDesc</p>";

          echo "</div>";
          // div class row

          echo "<div class=\"row\">";
            echo "<div class=\"col-md-8\">";
            if(sizeof($module[5]) <= 3){
              echo "<canvas id=\"donut$i\" width=\"950\" height=\"500\"</canvas>";
            }
            echo "<script type=\"text/javascript\"> new Chart(document.getElementById(\"donut$i\"), {";
              // Dans le script JS
              echo "type: 'doughnut', data: { labels: [\"";
              // Labels
              echo implode("\",\"", array_map(function($tab){return $tab[1];},$module[4]));
              echo "\"],";

              // Data
              echo "datasets: [{label: \"Module distribution\",backgroundColor: [\"#566637\", \"#C19944\", \"#8D8D8D\"],data: [";
                echo implode(",", array_map(function($tab){return $tab[0];},$module[4]));
              echo "]}]},options: {title: {display: true,text: 'Module distribution'}}";
            echo "});</script>";

            echo "</div>";
            if(sizeof($module[5]) <= 3){
              echo "<div class=\"col-md-4\">";
                echo "<div class=\"box-modules\">";
                  echo "Mean : ";
                  echo $module[3];
                echo "</div>";
              echo "</div>";
            }
            
          echo "</div>";
        echo "</div>";

        echo "<br/><br/>";

        echo "<table id=\"tableExams\" class=\"table table-bordered\">";
          echo "<thead>";
            echo "<tr>";
            
              echo "<th onclick=\"sortTable(0)\">Exam<button type=\"button\" name=\"exam\"></button></th>";
              echo "<th onclick=\"sortTable(1)\">Coefficient<button type=\"button\" name=\"coef\"></button></th>";
              echo "<th onclick=\"sortTable(2)\">Mark<button type=\"button\" name=\"mark\"></button></th>";
              if(sizeof($module[5]) > 3){
                echo "<th onclick=\"sortTable(3)\">Surname<button type=\"button\" name=\surname\"></button></th>";
                echo "<th onclick=\"sortTable(4)\">Name<button type=\"button\" name=\"name\"></button></th>";
  
              }
             
            echo "</tr>";
          echo "</thead>";
          echo "<tbody>";

          foreach ($module[5] as $noteLine) {
            echo "<tr>";
            foreach ($noteLine as $case) {
              echo "<td>";
              echo $case;
              echo "</td>";
            }
            echo "</tr>";
          }
          echo "</tbody>";
        echo "</table>";
      echo "</div>";
    echo "</div>";
    $i++;
  }
  ?>








</div>
