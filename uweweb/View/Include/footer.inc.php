
		 <!-- Footer -->
<footer class="page-footer font-small blue pt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-center">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="title-footer">UWE web developement</h5>
        <p class="text-footer">
        ANDRIEU Victor, BALBOA Marc, BERTAUX Corentin, CHAMAYOU Alexis,
        </br> VALERY Hugo, ZIRAK Soufiane
        </p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="title-footer">Navigation</h5>

        <ul class="list-unstyled">
          <li>
            <a href="index.php" style="color:#fff;">Home</a>
          </li>
        <?php
          if (!empty($_SESSION['connexion'])) {

            $role = User::getRole($_SESSION['connexion']);
        ?>
          <li>
            <a href="index.php?page=modules" style="color:#fff;">Modules</a>
          </li>
          <?php
            if ($role == 1 || $role == 3) {

            ?>
          <li>
            <a href="index.php?page=exams" style="color:#fff;">Exams</a>
          </li>
           <?php
            }

            if ($role ==3){
          ?>
          <li>
            <a href="index.php?page=administration" style="color:#fff;">Administration</a>
          </li>
          <?php

            }
          ?>
					<li>
						<a href="index.php?page=profile" style="color:#fff;">Profile</a>
					</li>
          <?php

          }
          else {

        ?>
          <li>
            <a href="index.php?page=login">Login</a>
          </li>
          <?php
          }
        ?>
        </ul>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 text-footer">© 2019 Copyright : Beziers Gang
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
