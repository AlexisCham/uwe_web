<!-- Main jumbotron for a primary marketing message or call to action -->
<?php 
    if (!empty($_SESSION['connexion'])) {
      //require("Model/user.class.php");
      $user = User::getUser($_SESSION['connexion']);
      for($index=0;$index < count($user);$index++) {
              $oneuser = $user[$index]
            
?>
<div class="jumbotron">
    <div class="container">
      <h1 class="display-3">Welcome <b><?php echo $oneuser->surname ?>,</b></h1>
      <h2>To UWE Marks ZBEG</h2>
      <p>  This website is designed for students to consult their exam details and module results.
      Authorized University members can add students, modules, marks, schedule exams and assign students to modules. 
      They also have access to an overview of the exam results for the different students.</p>
    </div>
  </div>
<?php 
      }
    } 
    else 
    {
?>
<div class="jumbotron">
    <div class="container">
      <h1 class="display-3">Welcome to UWE Marks ZBEG</h1>
      <p>  This website is designed for students to consult their exam details and module results.
      Authorized University members can add students, modules, marks, schedule exams and assign students to modules. 
      They also have access to an overview of the exam results for the different students.</p>
    </div>
  </div>

<?php 
    }
?>
  <div class="container">
    <!-- Example row of columns -->
    <div class="row">
      <div class="col-md-6">
        <h2>Please login in order to acces the full website</h2>
        <br/>
        <p>Once connected please take some time to check if there is any mistake on your personal profile and correct it.</p>
        
      </div>

      <div class="col-md-6">
        <h2>Any issue with the website?</h2>
        <br/>
        <p>If you encounter any problems while using our platform, feel free to let us know by contacting our hardworking team:
          <br/>
          <a href="mailto:balboa.marc@gmail.com">uwe.marks.zbeg@support.com</a>
        </p>
      </div>
    </div>
    <hr>


  </div> <!-- /container -->
</section>
