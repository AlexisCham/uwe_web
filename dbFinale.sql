-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 18 juil. 2019 à 14:12
-- Version du serveur :  10.3.16-MariaDB
-- Version de PHP :  7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `uwetest`
--

-- --------------------------------------------------------

--
-- Structure de la table `components`
--

CREATE TABLE `components` (
  `id` int(11) NOT NULL,
  `coefficient` int(11) NOT NULL,
  `examDate` datetime NOT NULL,
  `moduleId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `isResit` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `components`
--

INSERT INTO `components` (`id`, `coefficient`, `examDate`, `moduleId`, `typeId`, `isResit`) VALUES
(1, 3, '2019-07-30 00:00:00', 1, 1, 0),
(2, 2, '2019-08-01 00:00:00', 1, 2, 0),
(3, 5, '2019-08-02 00:00:00', 1, 3, 0),
(4, 5, '2019-08-03 00:00:00', 2, 1, 0),
(5, 5, '2019-08-04 00:00:00', 2, 2, 0),
(6, 6, '2019-08-06 00:00:00', 3, 2, 0),
(7, 4, '2019-08-07 00:00:00', 3, 3, 0),
(8, 5, '2019-08-11 00:00:00', 4, 1, 0),
(9, 5, '2019-08-12 00:00:00', 4, 1, 0),
(10, 10, '2019-07-18 00:00:00', 5, 1, 0),
(11, 5, '2019-08-18 00:00:00', 6, 1, 0),
(12, 5, '2019-08-19 00:00:00', 6, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `components_types`
--

CREATE TABLE `components_types` (
  `id` int(11) NOT NULL,
  `type` varchar(40) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `components_types`
--

INSERT INTO `components_types` (`id`, `type`) VALUES
(1, 'Assignment'),
(2, 'Lab Tests'),
(3, 'Written Exam');

-- --------------------------------------------------------

--
-- Structure de la table `enrolments`
--

CREATE TABLE `enrolments` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `moduleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `enrolments`
--

INSERT INTO `enrolments` (`id`, `userId`, `moduleId`) VALUES
(1, 4, 1),
(2, 4, 2),
(3, 4, 3),
(4, 4, 4),
(5, 4, 5),
(6, 4, 6),
(7, 5, 1),
(8, 5, 2),
(9, 5, 3),
(10, 5, 4),
(11, 5, 5),
(12, 5, 6),
(13, 6, 1),
(14, 6, 2),
(15, 6, 3),
(16, 6, 4),
(17, 6, 5),
(18, 6, 6),
(19, 7, 1),
(20, 7, 2),
(21, 7, 3),
(22, 7, 4),
(23, 7, 5),
(24, 7, 6),
(25, 8, 1),
(26, 8, 2),
(27, 8, 3),
(28, 8, 4),
(29, 8, 5),
(30, 8, 6),
(31, 9, 1),
(32, 9, 2),
(33, 9, 3),
(34, 9, 4),
(35, 9, 5),
(36, 9, 6),
(37, 10, 1),
(38, 10, 2),
(39, 10, 3),
(40, 10, 4),
(41, 10, 5),
(42, 10, 6),
(43, 11, 1),
(44, 11, 2),
(45, 11, 3),
(46, 11, 4),
(47, 11, 5),
(48, 11, 6),
(49, 12, 1),
(50, 12, 2),
(51, 12, 3),
(52, 12, 4),
(53, 12, 5),
(54, 12, 6),
(55, 13, 1),
(56, 13, 2),
(57, 13, 3),
(58, 13, 4),
(59, 13, 5),
(60, 13, 6),
(61, 14, 1),
(62, 14, 2),
(63, 14, 3),
(64, 14, 4),
(65, 14, 5),
(66, 14, 6),
(67, 15, 1),
(68, 15, 2),
(69, 15, 3),
(70, 15, 4),
(71, 15, 5),
(72, 15, 6),
(73, 16, 1),
(74, 16, 2),
(75, 16, 3),
(76, 16, 4),
(77, 16, 5),
(78, 16, 6),
(79, 17, 1),
(80, 17, 2),
(81, 17, 3),
(82, 17, 4),
(83, 17, 5),
(84, 17, 6),
(85, 18, 1),
(86, 18, 2),
(87, 18, 3),
(88, 18, 4),
(89, 18, 5),
(90, 18, 6),
(91, 19, 1),
(92, 19, 2),
(93, 19, 3),
(94, 19, 4),
(95, 19, 5),
(96, 19, 6),
(97, 20, 1),
(98, 20, 2),
(99, 20, 3),
(100, 20, 4),
(101, 20, 5),
(102, 20, 6),
(103, 21, 1),
(104, 21, 2),
(105, 21, 3),
(106, 21, 4),
(107, 21, 5),
(108, 21, 6),
(109, 22, 1),
(110, 22, 2),
(111, 22, 3),
(112, 22, 4),
(113, 22, 5),
(114, 22, 6),
(115, 23, 1),
(116, 23, 2),
(117, 23, 3),
(118, 23, 4),
(119, 23, 5),
(120, 23, 6),
(121, 24, 1),
(122, 24, 2),
(123, 24, 3),
(124, 24, 4),
(125, 24, 5),
(126, 24, 6),
(127, 25, 1),
(128, 25, 2),
(129, 25, 3),
(130, 25, 4),
(131, 25, 5),
(132, 25, 6),
(133, 26, 1),
(134, 26, 2),
(135, 26, 3),
(136, 26, 4),
(137, 26, 5),
(138, 26, 6),
(139, 27, 1),
(140, 27, 2),
(141, 27, 3),
(142, 27, 4),
(143, 27, 5),
(144, 27, 6),
(145, 28, 1),
(146, 28, 2),
(147, 28, 3),
(148, 28, 4),
(149, 28, 5),
(150, 28, 6),
(151, 29, 1),
(152, 29, 2),
(153, 29, 3),
(154, 29, 4),
(155, 29, 5),
(156, 29, 6),
(157, 30, 1),
(158, 30, 2),
(159, 30, 3),
(160, 30, 4),
(161, 30, 5),
(162, 30, 6),
(163, 31, 1),
(164, 31, 2),
(165, 31, 3),
(166, 31, 4),
(167, 31, 5),
(168, 31, 6),
(169, 32, 1),
(170, 32, 2),
(171, 32, 3),
(172, 32, 4),
(173, 32, 5),
(174, 32, 6),
(175, 33, 1),
(176, 33, 2),
(177, 33, 3),
(178, 33, 4),
(179, 33, 5),
(180, 33, 6),
(181, 34, 1),
(182, 34, 2),
(183, 34, 3),
(184, 34, 4),
(185, 34, 5),
(186, 34, 6),
(187, 35, 1),
(188, 35, 2),
(189, 35, 3),
(190, 35, 4),
(191, 35, 5),
(192, 35, 6),
(193, 36, 1),
(194, 36, 2),
(195, 36, 3),
(196, 36, 4),
(197, 36, 5),
(198, 36, 6),
(199, 37, 1),
(200, 37, 2),
(201, 37, 3),
(202, 37, 4),
(203, 37, 5),
(204, 37, 6),
(205, 38, 1),
(206, 38, 2),
(207, 38, 3),
(208, 38, 4),
(209, 38, 5),
(210, 38, 6),
(211, 39, 1),
(212, 39, 2),
(213, 39, 3),
(214, 39, 4),
(215, 39, 5),
(216, 39, 6),
(217, 40, 1),
(218, 40, 2),
(219, 40, 3),
(220, 40, 4),
(221, 40, 5),
(222, 40, 6),
(223, 41, 1),
(224, 41, 2),
(225, 41, 3),
(226, 41, 4),
(227, 41, 5),
(228, 41, 6),
(229, 42, 1),
(230, 42, 2),
(231, 42, 3),
(232, 42, 4),
(233, 42, 5),
(234, 42, 6),
(235, 43, 1),
(236, 43, 2),
(237, 43, 3),
(238, 43, 4),
(239, 43, 5),
(240, 43, 6),
(241, 44, 1),
(242, 44, 2),
(243, 44, 3),
(244, 44, 4),
(245, 44, 5),
(246, 44, 6),
(247, 45, 1),
(248, 45, 2),
(249, 45, 3),
(250, 45, 4),
(251, 45, 5),
(252, 45, 6),
(253, 46, 1),
(254, 46, 2),
(255, 46, 3),
(256, 46, 4),
(257, 46, 5),
(258, 46, 6),
(259, 47, 1),
(260, 47, 2),
(261, 47, 3),
(262, 47, 4),
(263, 47, 5),
(264, 47, 6),
(265, 48, 1),
(266, 48, 2),
(267, 48, 3),
(268, 48, 4),
(269, 48, 5),
(270, 48, 6),
(271, 49, 1),
(272, 49, 2),
(273, 49, 3),
(274, 49, 4),
(275, 49, 5),
(276, 49, 6),
(277, 50, 1),
(278, 50, 2),
(279, 50, 3),
(280, 50, 4),
(281, 50, 5),
(282, 50, 6),
(283, 51, 1),
(284, 51, 2),
(285, 51, 3),
(286, 51, 4),
(287, 51, 5),
(288, 51, 6),
(289, 53, 1);

-- --------------------------------------------------------

--
-- Structure de la table `marks`
--

CREATE TABLE `marks` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `componentId` int(11) NOT NULL,
  `mark` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `marks`
--

INSERT INTO `marks` (`id`, `userId`, `componentId`, `mark`) VALUES
(5, 4, 1, 34),
(6, 4, 2, 75),
(7, 4, 3, 37),
(8, 4, 4, 51),
(9, 4, 5, 7),
(10, 4, 6, 66),
(11, 4, 7, 11),
(12, 4, 8, 76),
(13, 4, 9, 72),
(14, 4, 10, 27),
(15, 4, 11, 68),
(16, 4, 12, 83),
(17, 5, 1, 35),
(18, 5, 2, 28),
(19, 5, 3, 97),
(20, 5, 4, 80),
(21, 5, 5, 32),
(22, 5, 6, 36),
(23, 5, 7, 48),
(24, 5, 8, 43),
(25, 5, 9, 83),
(26, 5, 10, 55),
(27, 5, 11, 34),
(28, 5, 12, 74),
(29, 6, 1, 82),
(30, 6, 2, 27),
(31, 6, 3, 21),
(32, 6, 4, 64),
(33, 6, 5, 54),
(34, 6, 6, 63),
(35, 6, 7, 11),
(36, 6, 8, 56),
(37, 6, 9, 48),
(38, 6, 10, 74),
(39, 6, 11, 27),
(40, 6, 12, 46),
(41, 7, 1, 72),
(42, 7, 2, 10),
(43, 7, 3, 0),
(44, 7, 4, 47),
(45, 7, 5, 37),
(46, 7, 6, 29),
(47, 7, 7, 73),
(48, 7, 8, 70),
(49, 7, 9, 65),
(50, 7, 10, 70),
(51, 7, 11, 87),
(52, 7, 12, 41),
(53, 8, 1, 89),
(54, 8, 2, 22),
(55, 8, 3, 70),
(56, 8, 4, 40),
(57, 8, 5, 85),
(58, 8, 6, 76),
(59, 8, 7, 87),
(60, 8, 8, 68),
(61, 8, 9, 47),
(62, 8, 10, 42),
(63, 8, 11, 26),
(64, 8, 12, 97),
(65, 9, 1, 42),
(66, 9, 2, 96),
(67, 9, 3, 60),
(68, 9, 4, 69),
(69, 9, 5, 91),
(70, 9, 6, 47),
(71, 9, 7, 67),
(72, 9, 8, 51),
(73, 9, 9, 31),
(74, 9, 10, 71),
(75, 9, 11, 35),
(76, 9, 12, 58),
(77, 10, 1, 11),
(78, 10, 2, 62),
(79, 10, 3, 11),
(80, 10, 4, 80),
(81, 10, 5, 52),
(82, 10, 6, 44),
(83, 10, 7, 5),
(84, 10, 8, 19),
(85, 10, 9, 42),
(86, 10, 10, 79),
(87, 10, 11, 44),
(88, 10, 12, 46),
(89, 11, 1, 6),
(90, 11, 2, 3),
(91, 11, 3, 76),
(92, 11, 4, 42),
(93, 11, 5, 21),
(94, 11, 6, 57),
(95, 11, 7, 8),
(96, 11, 8, 4),
(97, 11, 9, 69),
(98, 11, 10, 53),
(99, 11, 11, 69),
(100, 11, 12, 8),
(101, 12, 1, 41),
(102, 12, 2, 71),
(103, 12, 3, 82),
(104, 12, 4, 55),
(105, 12, 5, 75),
(106, 12, 6, 24),
(107, 12, 7, 17),
(108, 12, 8, 78),
(109, 12, 9, 88),
(110, 12, 10, 100),
(111, 12, 11, 46),
(112, 12, 12, 7),
(113, 13, 1, 32),
(114, 13, 2, 73),
(115, 13, 3, 29),
(116, 13, 4, 71),
(117, 13, 5, 30),
(118, 13, 6, 43),
(119, 13, 7, 34),
(120, 13, 8, 79),
(121, 13, 9, 67),
(122, 13, 10, 45),
(123, 13, 11, 26),
(124, 13, 12, 57),
(125, 14, 1, 12),
(126, 14, 2, 26),
(127, 14, 3, 70),
(128, 14, 4, 29),
(129, 14, 5, 69),
(130, 14, 6, 99),
(131, 14, 7, 51),
(132, 14, 8, 93),
(133, 14, 9, 75),
(134, 14, 10, 88),
(135, 14, 11, 22),
(136, 14, 12, 11),
(137, 15, 1, 31),
(138, 15, 2, 56),
(139, 15, 3, 53),
(140, 15, 4, 66),
(141, 15, 5, 9),
(142, 15, 6, 1),
(143, 15, 7, 10),
(144, 15, 8, 52),
(145, 15, 9, 56),
(146, 15, 10, 89),
(147, 15, 11, 10),
(148, 15, 12, 92),
(149, 16, 1, 9),
(150, 16, 2, 2),
(151, 16, 3, 54),
(152, 16, 4, 37),
(153, 16, 5, 29),
(154, 16, 6, 50),
(155, 16, 7, 56),
(156, 16, 8, 75),
(157, 16, 9, 19),
(158, 16, 10, 46),
(159, 16, 11, 15),
(160, 16, 12, 69),
(161, 17, 1, 19),
(162, 17, 2, 40),
(163, 17, 3, 89),
(164, 17, 4, 86),
(165, 17, 5, 72),
(166, 17, 6, 96),
(167, 17, 7, 37),
(168, 17, 8, 24),
(169, 17, 9, 74),
(170, 17, 10, 50),
(171, 17, 11, 18),
(172, 17, 12, 57),
(173, 18, 1, 88),
(174, 18, 2, 87),
(175, 18, 3, 92),
(176, 18, 4, 45),
(177, 18, 5, 64),
(178, 18, 6, 15),
(179, 18, 7, 77),
(180, 18, 8, 50),
(181, 18, 9, 43),
(182, 18, 10, 14),
(183, 18, 11, 60),
(184, 18, 12, 49),
(185, 19, 1, 62),
(186, 19, 2, 27),
(187, 19, 3, 74),
(188, 19, 4, 92),
(189, 19, 5, 1),
(190, 19, 6, 79),
(191, 19, 7, 52),
(192, 19, 8, 44),
(193, 19, 9, 44),
(194, 19, 10, 24),
(195, 19, 11, 12),
(196, 19, 12, 51),
(197, 20, 1, 47),
(198, 20, 2, 96),
(199, 20, 3, 65),
(200, 20, 4, 40),
(201, 20, 5, 36),
(202, 20, 6, 98),
(203, 20, 7, 46),
(204, 20, 8, 50),
(205, 20, 9, 41),
(206, 20, 10, 91),
(207, 20, 11, 98),
(208, 20, 12, 10),
(209, 21, 1, 28),
(210, 21, 2, 41),
(211, 21, 3, 2),
(212, 21, 4, 5),
(213, 21, 5, 64),
(214, 21, 6, 71),
(215, 21, 7, 22),
(216, 21, 8, 84),
(217, 21, 9, 69),
(218, 21, 10, 75),
(219, 21, 11, 13),
(220, 21, 12, 82),
(221, 22, 1, 56),
(222, 22, 2, 11),
(223, 22, 3, 9),
(224, 22, 4, 40),
(225, 22, 5, 13),
(226, 22, 6, 29),
(227, 22, 7, 39),
(228, 22, 8, 72),
(229, 22, 9, 70),
(230, 22, 10, 99),
(231, 22, 11, 45),
(232, 22, 12, 16),
(233, 23, 1, 2),
(234, 23, 2, 97),
(235, 23, 3, 36),
(236, 23, 4, 28),
(237, 23, 5, 37),
(238, 23, 6, 73),
(239, 23, 7, 53),
(240, 23, 8, 4),
(241, 23, 9, 18),
(242, 23, 10, 97),
(243, 23, 11, 7),
(244, 23, 12, 75),
(245, 24, 1, 59),
(246, 24, 2, 2),
(247, 24, 3, 46),
(248, 24, 4, 89),
(249, 24, 5, 5),
(250, 24, 6, 25),
(251, 24, 7, 57),
(252, 24, 8, 71),
(253, 24, 9, 40),
(254, 24, 10, 61),
(255, 24, 11, 59),
(256, 24, 12, 33),
(257, 25, 1, 10),
(258, 25, 2, 61),
(259, 25, 3, 82),
(260, 25, 4, 26),
(261, 25, 5, 91),
(262, 25, 6, 60),
(263, 25, 7, 40),
(264, 25, 8, 66),
(265, 25, 9, 65),
(266, 25, 10, 48),
(267, 25, 11, 62),
(268, 25, 12, 24),
(269, 26, 1, 76),
(270, 26, 2, 59),
(271, 26, 3, 18),
(272, 26, 4, 26),
(273, 26, 5, 43),
(274, 26, 6, 2),
(275, 26, 7, 75),
(276, 26, 8, 92),
(277, 26, 9, 1),
(278, 26, 10, 97),
(279, 26, 11, 42),
(280, 26, 12, 35),
(281, 27, 1, 19),
(282, 27, 2, 98),
(283, 27, 3, 5),
(284, 27, 4, 36),
(285, 27, 5, 91),
(286, 27, 6, 82),
(287, 27, 7, 27),
(288, 27, 8, 76),
(289, 27, 9, 73),
(290, 27, 10, 66),
(291, 27, 11, 36),
(292, 27, 12, 35),
(293, 28, 1, 13),
(294, 28, 2, 16),
(295, 28, 3, 82),
(296, 28, 4, 93),
(297, 28, 5, 41),
(298, 28, 6, 25),
(299, 28, 7, 99),
(300, 28, 8, 99),
(301, 28, 9, 67),
(302, 28, 10, 37),
(303, 28, 11, 68),
(304, 28, 12, 20),
(305, 29, 1, 67),
(306, 29, 2, 87),
(307, 29, 3, 9),
(308, 29, 4, 63),
(309, 29, 5, 66),
(310, 29, 6, 8),
(311, 29, 7, 90),
(312, 29, 8, 30),
(313, 29, 9, 66),
(314, 29, 10, 12),
(315, 29, 11, 39),
(316, 29, 12, 2),
(317, 30, 1, 64),
(318, 30, 2, 93),
(319, 30, 3, 98),
(320, 30, 4, 96),
(321, 30, 5, 29),
(322, 30, 6, 45),
(323, 30, 7, 70),
(324, 30, 8, 0),
(325, 30, 9, 45),
(326, 30, 10, 62),
(327, 30, 11, 84),
(328, 30, 12, 78),
(329, 31, 1, 69),
(330, 31, 2, 0),
(331, 31, 3, 51),
(332, 31, 4, 50),
(333, 31, 5, 62),
(334, 31, 6, 35),
(335, 31, 7, 22),
(336, 31, 8, 7),
(337, 31, 9, 100),
(338, 31, 10, 67),
(339, 31, 11, 16),
(340, 31, 12, 26),
(341, 32, 1, 86),
(342, 32, 2, 67),
(343, 32, 3, 70),
(344, 32, 4, 4),
(345, 32, 5, 9),
(346, 32, 6, 17),
(347, 32, 7, 9),
(348, 32, 8, 95),
(349, 32, 9, 34),
(350, 32, 10, 73),
(351, 32, 11, 77),
(352, 32, 12, 73),
(353, 33, 1, 88),
(354, 33, 2, 85),
(355, 33, 3, 97),
(356, 33, 4, 98),
(357, 33, 5, 51),
(358, 33, 6, 8),
(359, 33, 7, 74),
(360, 33, 8, 65),
(361, 33, 9, 44),
(362, 33, 10, 96),
(363, 33, 11, 70),
(364, 33, 12, 47),
(365, 34, 1, 46),
(366, 34, 2, 88),
(367, 34, 3, 74),
(368, 34, 4, 53),
(369, 34, 5, 10),
(370, 34, 6, 77),
(371, 34, 7, 10),
(372, 34, 8, 73),
(373, 34, 9, 97),
(374, 34, 10, 48),
(375, 34, 11, 12),
(376, 34, 12, 83),
(377, 35, 1, 38),
(378, 35, 2, 46),
(379, 35, 3, 32),
(380, 35, 4, 22),
(381, 35, 5, 8),
(382, 35, 6, 40),
(383, 35, 7, 21),
(384, 35, 8, 74),
(385, 35, 9, 38),
(386, 35, 10, 15),
(387, 35, 11, 86),
(388, 35, 12, 20),
(389, 36, 1, 40),
(390, 36, 2, 55),
(391, 36, 3, 9),
(392, 36, 4, 75),
(393, 36, 5, 34),
(394, 36, 6, 19),
(395, 36, 7, 45),
(396, 36, 8, 73),
(397, 36, 9, 88),
(398, 36, 10, 58),
(399, 36, 11, 85),
(400, 36, 12, 81),
(401, 37, 1, 73),
(402, 37, 2, 81),
(403, 37, 3, 58),
(404, 37, 4, 14),
(405, 37, 5, 48),
(406, 37, 6, 87),
(407, 37, 7, 0),
(408, 37, 8, 69),
(409, 37, 9, 66),
(410, 37, 10, 33),
(411, 37, 11, 74),
(412, 37, 12, 20),
(413, 38, 1, 49),
(414, 38, 2, 64),
(415, 38, 3, 11),
(416, 38, 4, 72),
(417, 38, 5, 9),
(418, 38, 6, 22),
(419, 38, 7, 40),
(420, 38, 8, 92),
(421, 38, 9, 88),
(422, 38, 10, 11),
(423, 38, 11, 99),
(424, 38, 12, 59),
(425, 39, 1, 87),
(426, 39, 2, 5),
(427, 39, 3, 3),
(428, 39, 4, 80),
(429, 39, 5, 18),
(430, 39, 6, 55),
(431, 39, 7, 6),
(432, 39, 8, 45),
(433, 39, 9, 96),
(434, 39, 10, 50),
(435, 39, 11, 91),
(436, 39, 12, 53),
(437, 40, 1, 16),
(438, 40, 2, 64),
(439, 40, 3, 73),
(440, 40, 4, 73),
(441, 40, 5, 69),
(442, 40, 6, 49),
(443, 40, 7, 43),
(444, 40, 8, 92),
(445, 40, 9, 39),
(446, 40, 10, 16),
(447, 40, 11, 42),
(448, 40, 12, 6),
(449, 41, 1, 20),
(450, 41, 2, 100),
(451, 41, 3, 52),
(452, 41, 4, 87),
(453, 41, 5, 71),
(454, 41, 6, 43),
(455, 41, 7, 74),
(456, 41, 8, 71),
(457, 41, 9, 32),
(458, 41, 10, 52),
(459, 41, 11, 26),
(460, 41, 12, 7),
(461, 42, 1, 45),
(462, 42, 2, 44),
(463, 42, 3, 46),
(464, 42, 4, 42),
(465, 42, 5, 48),
(466, 42, 6, 73),
(467, 42, 7, 70),
(468, 42, 8, 67),
(469, 42, 9, 53),
(470, 42, 10, 91),
(471, 42, 11, 32),
(472, 42, 12, 27),
(473, 43, 1, 16),
(474, 43, 2, 68),
(475, 43, 3, 83),
(476, 43, 4, 10),
(477, 43, 5, 51),
(478, 43, 6, 98),
(479, 43, 7, 77),
(480, 43, 8, 29),
(481, 43, 9, 46),
(482, 43, 10, 96),
(483, 43, 11, 93),
(484, 43, 12, 51),
(485, 44, 1, 34),
(486, 44, 2, 66),
(487, 44, 3, 17),
(488, 44, 4, 87),
(489, 44, 5, 68),
(490, 44, 6, 19),
(491, 44, 7, 80),
(492, 44, 8, 48),
(493, 44, 9, 87),
(494, 44, 10, 76),
(495, 44, 11, 73),
(496, 44, 12, 26),
(497, 45, 1, 8),
(498, 45, 2, 78),
(499, 45, 3, 3),
(500, 45, 4, 88),
(501, 45, 5, 54),
(502, 45, 6, 51),
(503, 45, 7, 87),
(504, 45, 8, 57),
(505, 45, 9, 57),
(506, 45, 10, 5),
(507, 45, 11, 61),
(508, 45, 12, 16),
(509, 46, 1, 21),
(510, 46, 2, 54),
(511, 46, 3, 45),
(512, 46, 4, 52),
(513, 46, 5, 47),
(514, 46, 6, 81),
(515, 46, 7, 37),
(516, 46, 8, 76),
(517, 46, 9, 65),
(518, 46, 10, 64),
(519, 46, 11, 24),
(520, 46, 12, 87),
(521, 47, 1, 59),
(522, 47, 2, 98),
(523, 47, 3, 49),
(524, 47, 4, 51),
(525, 47, 5, 50),
(526, 47, 6, 7),
(527, 47, 7, 9),
(528, 47, 8, 51),
(529, 47, 9, 9),
(530, 47, 10, 50),
(531, 47, 11, 99),
(532, 47, 12, 91),
(533, 48, 1, 17),
(534, 48, 2, 58),
(535, 48, 3, 40),
(536, 48, 4, 39),
(537, 48, 5, 35),
(538, 48, 6, 91),
(539, 48, 7, 37),
(540, 48, 8, 53),
(541, 48, 9, 81),
(542, 48, 10, 16),
(543, 48, 11, 53),
(544, 48, 12, 33),
(545, 49, 1, 83),
(546, 49, 2, 75),
(547, 49, 3, 9),
(548, 49, 4, 50),
(549, 49, 5, 58),
(550, 49, 6, 44),
(551, 49, 7, 99),
(552, 49, 8, 57),
(553, 49, 9, 5),
(554, 49, 10, 51),
(555, 49, 11, 60),
(556, 49, 12, 84),
(557, 50, 1, 1),
(558, 50, 2, 68),
(559, 50, 3, 83),
(560, 50, 4, 36),
(561, 50, 5, 10),
(562, 50, 6, 57),
(563, 50, 7, 97),
(564, 50, 8, 9),
(565, 50, 9, 57),
(566, 50, 10, 55),
(567, 50, 11, 64),
(568, 50, 12, 12),
(569, 51, 1, 31),
(570, 51, 2, 73),
(571, 51, 3, 40),
(572, 51, 4, 83),
(573, 51, 5, 92),
(574, 51, 6, 53),
(575, 51, 7, 29),
(576, 51, 8, 19),
(577, 51, 9, 8),
(578, 51, 10, 92),
(579, 51, 11, 14),
(580, 51, 12, 42);

-- --------------------------------------------------------

--
-- Structure de la table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8_bin NOT NULL,
  `details` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `modules`
--

INSERT INTO `modules` (`id`, `name`, `details`) VALUES
(1, 'WP', 'Web Programming'),
(2, 'WD', 'Web Design'),
(3, 'CMS', 'Content Management System'),
(4, 'LESPI', 'Legal Ethical Social and Professional Issues'),
(5, 'WDF', 'Web Development Frameworks'),
(6, 'WT', 'Web Technologies');

-- --------------------------------------------------------

--
-- Structure de la table `modules_results`
--

CREATE TABLE `modules_results` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `moduleId` int(11) NOT NULL,
  `grade` varchar(5) COLLATE utf8_bin NOT NULL,
  `resit` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `modules_results`
--

INSERT INTO `modules_results` (`id`, `userId`, `moduleId`, `grade`, `resit`) VALUES
(2, 4, 1, 'D', 0),
(3, 4, 2, 'F', 1),
(4, 4, 3, 'D', 0),
(5, 4, 4, 'A', 0),
(6, 4, 5, 'F', 1),
(7, 4, 6, 'A', 0),
(8, 5, 1, 'A++', 0),
(9, 5, 2, 'C', 0),
(10, 5, 3, 'D', 0),
(11, 5, 4, 'B', 0),
(12, 5, 5, 'C', 0),
(13, 5, 6, 'C', 0),
(14, 6, 1, 'A++', 0),
(15, 6, 2, 'C', 0),
(16, 6, 3, 'D', 0),
(17, 6, 4, 'C', 0),
(18, 6, 5, 'A', 0),
(19, 6, 6, 'F', 1),
(20, 7, 1, 'B', 0),
(21, 7, 2, 'D', 0),
(22, 7, 3, 'D', 0),
(23, 7, 4, 'B', 0),
(24, 7, 5, 'A', 0),
(25, 7, 6, 'B', 0),
(26, 8, 1, 'A++', 0),
(27, 8, 2, 'B', 0),
(28, 8, 3, 'A+', 0),
(29, 8, 4, 'C', 0),
(30, 8, 5, 'D', 0),
(31, 8, 6, 'B', 0),
(32, 9, 1, 'A++', 0),
(33, 9, 2, 'A+', 0),
(34, 9, 3, 'C', 0),
(35, 9, 4, 'D', 0),
(36, 9, 5, 'A', 0),
(37, 9, 6, 'D', 0),
(38, 10, 1, 'B', 0),
(39, 10, 2, 'B', 0),
(40, 10, 3, 'F', 1),
(41, 10, 4, 'F', 1),
(42, 10, 5, 'A', 0),
(43, 10, 6, 'D', 0),
(44, 11, 1, 'A+', 0),
(45, 11, 2, 'F', 1),
(46, 11, 3, 'F', 1),
(47, 11, 4, 'F', 1),
(48, 11, 5, 'C', 0),
(49, 11, 6, 'F', 1),
(50, 12, 1, 'A++', 0),
(51, 12, 2, 'B', 0),
(52, 12, 3, 'F', 1),
(53, 12, 4, 'A+', 0),
(54, 12, 5, 'A++', 0),
(55, 12, 6, 'F', 1),
(56, 13, 1, 'B', 0),
(57, 13, 2, 'C', 0),
(58, 13, 3, 'F', 1),
(59, 13, 4, 'A', 0),
(60, 13, 5, 'D', 0),
(61, 13, 6, 'D', 0),
(62, 14, 1, 'A+', 0),
(63, 14, 2, 'D', 0),
(64, 14, 3, 'D', 0),
(65, 14, 4, 'A+', 0),
(66, 14, 5, 'A+', 0),
(67, 14, 6, 'F', 1),
(68, 15, 1, 'B', 0),
(69, 15, 2, 'F', 1),
(70, 15, 3, 'F', 1),
(71, 15, 4, 'C', 0),
(72, 15, 5, 'C', 0),
(73, 15, 6, 'C', 0),
(74, 16, 1, 'A+', 0),
(75, 16, 2, 'F', 1),
(76, 16, 3, 'C', 0),
(77, 16, 4, 'D', 0),
(78, 16, 5, 'D', 0),
(79, 16, 6, 'D', 0),
(80, 17, 1, 'A++', 0),
(81, 17, 2, 'A', 0),
(82, 17, 3, 'A', 0),
(83, 17, 4, 'D', 0),
(84, 17, 5, 'C', 0),
(85, 17, 6, 'F', 1),
(86, 18, 1, 'A++', 0),
(87, 18, 2, 'C', 0),
(88, 18, 3, 'F', 1),
(89, 18, 4, 'D', 0),
(90, 18, 5, 'F', 1),
(91, 18, 6, 'C', 0),
(92, 19, 1, 'A++', 0),
(93, 19, 2, 'D', 0),
(94, 19, 3, 'B', 0),
(95, 19, 4, 'D', 0),
(96, 19, 5, 'F', 1),
(97, 19, 6, 'F', 1),
(98, 20, 1, 'A++', 0),
(99, 20, 2, 'F', 1),
(100, 20, 3, 'A', 0),
(101, 20, 4, 'D', 0),
(102, 20, 5, 'A++', 0),
(103, 20, 6, 'C', 0),
(104, 21, 1, 'A', 0),
(105, 21, 2, 'F', 1),
(106, 21, 3, 'C', 0),
(107, 21, 4, 'A', 0),
(108, 21, 5, 'A', 0),
(109, 21, 6, 'D', 0),
(110, 22, 1, 'A', 0),
(111, 22, 2, 'F', 1),
(112, 22, 3, 'F', 1),
(113, 22, 4, 'A', 0),
(114, 22, 5, 'A++', 0),
(115, 22, 6, 'F', 1),
(116, 23, 1, 'B', 0),
(117, 23, 2, 'F', 1),
(118, 23, 3, 'B', 0),
(119, 23, 4, 'F', 1),
(120, 23, 5, 'A++', 0),
(121, 23, 6, 'D', 0),
(122, 24, 1, 'A+', 0),
(123, 24, 2, 'D', 0),
(124, 24, 3, 'F', 1),
(125, 24, 4, 'C', 0),
(126, 24, 5, 'B', 0),
(127, 24, 6, 'D', 0),
(128, 25, 1, 'A++', 0),
(129, 25, 2, 'C', 0),
(130, 25, 3, 'C', 0),
(131, 25, 4, 'B', 0),
(132, 25, 5, 'D', 0),
(133, 25, 6, 'D', 0),
(134, 26, 1, 'A+', 0),
(135, 26, 2, 'F', 1),
(136, 26, 3, 'F', 1),
(137, 26, 4, 'D', 0),
(138, 26, 5, 'A++', 0),
(139, 26, 6, 'F', 1),
(140, 27, 1, 'B', 0),
(141, 27, 2, 'B', 0),
(142, 27, 3, 'B', 0),
(143, 27, 4, 'A', 0),
(144, 27, 5, 'B', 0),
(145, 27, 6, 'F', 1),
(146, 28, 1, 'A+', 0),
(147, 28, 2, 'B', 0),
(148, 28, 3, 'C', 0),
(149, 28, 4, 'A+', 0),
(150, 28, 5, 'F', 1),
(151, 28, 6, 'D', 0),
(152, 29, 1, 'A+', 0),
(153, 29, 2, 'B', 0),
(154, 29, 3, 'D', 0),
(155, 29, 4, 'D', 0),
(156, 29, 5, 'F', 1),
(157, 29, 6, 'F', 1),
(158, 30, 1, 'A++', 0),
(159, 30, 2, 'B', 0),
(160, 30, 3, 'C', 0),
(161, 30, 4, 'F', 1),
(162, 30, 5, 'B', 0),
(163, 30, 6, 'A+', 0),
(164, 31, 1, 'A++', 0),
(165, 31, 2, 'C', 0),
(166, 31, 3, 'F', 1),
(167, 31, 4, 'C', 0),
(168, 31, 5, 'B', 0),
(169, 31, 6, 'F', 1),
(170, 32, 1, 'A++', 0),
(171, 32, 2, 'F', 1),
(172, 32, 3, 'F', 1),
(173, 32, 4, 'B', 0),
(174, 32, 5, 'A', 0),
(175, 32, 6, 'A', 0),
(176, 33, 1, 'A++', 0),
(177, 33, 2, 'A', 0),
(178, 33, 3, 'F', 1),
(179, 33, 4, 'C', 0),
(180, 33, 5, 'A++', 0),
(181, 33, 6, 'C', 0),
(182, 34, 1, 'A++', 0),
(183, 34, 2, 'F', 1),
(184, 34, 3, 'C', 0),
(185, 34, 4, 'A+', 0),
(186, 34, 5, 'D', 0),
(187, 34, 6, 'D', 0),
(188, 35, 1, 'A+', 0),
(189, 35, 2, 'F', 1),
(190, 35, 3, 'F', 1),
(191, 35, 4, 'C', 0),
(192, 35, 5, 'F', 1),
(193, 35, 6, 'C', 0),
(194, 36, 1, 'A+', 0),
(195, 36, 2, 'C', 0),
(196, 36, 3, 'F', 1),
(197, 36, 4, 'A+', 0),
(198, 36, 5, 'C', 0),
(199, 36, 6, 'A+', 0),
(200, 37, 1, 'A++', 0),
(201, 37, 2, 'F', 1),
(202, 37, 3, 'C', 0),
(203, 37, 4, 'B', 0),
(204, 37, 5, 'F', 1),
(205, 37, 6, 'D', 0),
(206, 38, 1, 'A+', 0),
(207, 38, 2, 'D', 0),
(208, 38, 3, 'F', 1),
(209, 38, 4, 'A++', 0),
(210, 38, 5, 'F', 1),
(211, 38, 6, 'A', 0),
(212, 39, 1, 'A++', 0),
(213, 39, 2, 'D', 0),
(214, 39, 3, 'F', 1),
(215, 39, 4, 'A', 0),
(216, 39, 5, 'C', 0),
(217, 39, 6, 'A', 0),
(218, 40, 1, 'A++', 0),
(219, 40, 2, 'A', 0),
(220, 40, 3, 'D', 0),
(221, 40, 4, 'B', 0),
(222, 40, 5, 'F', 1),
(223, 40, 6, 'F', 1),
(224, 41, 1, 'A', 0),
(225, 41, 2, 'A', 0),
(226, 41, 3, 'C', 0),
(227, 41, 4, 'C', 0),
(228, 41, 5, 'C', 0),
(229, 41, 6, 'F', 1),
(230, 42, 1, 'B', 0),
(231, 42, 2, 'D', 0),
(232, 42, 3, 'A', 0),
(233, 42, 4, 'B', 0),
(234, 42, 5, 'A++', 0),
(235, 42, 6, 'F', 1),
(236, 43, 1, 'F', 1),
(237, 43, 2, 'F', 1),
(238, 43, 3, 'F', 1),
(239, 43, 4, 'F', 1),
(240, 43, 5, 'A++', 0),
(241, 43, 6, 'A', 0),
(242, 44, 1, 'A++', 0),
(243, 44, 2, 'A', 0),
(244, 44, 3, 'D', 0),
(245, 44, 4, 'B', 0),
(246, 44, 5, 'A', 0),
(247, 44, 6, 'A', 0),
(248, 45, 1, 'B', 0),
(249, 45, 2, 'A', 0),
(250, 45, 3, 'B', 0),
(251, 45, 4, 'C', 0),
(252, 45, 5, 'F', 1),
(253, 45, 6, 'F', 1),
(254, 46, 1, 'A', 0),
(255, 46, 2, 'A', 0),
(256, 46, 3, 'B', 0),
(257, 46, 4, 'A', 0),
(258, 46, 5, 'B', 0),
(259, 46, 6, 'C', 0),
(260, 47, 1, 'A++', 0),
(261, 47, 2, 'C', 0),
(262, 47, 3, 'F', 1),
(263, 47, 4, 'F', 1),
(264, 47, 5, 'C', 0),
(265, 47, 6, 'A++', 0),
(266, 48, 1, 'A++', 0),
(267, 48, 2, 'F', 1),
(268, 48, 3, 'F', 1),
(269, 48, 4, 'B', 0),
(270, 48, 5, 'F', 1),
(271, 48, 6, 'D', 0),
(272, 49, 1, 'A+', 0),
(273, 49, 2, 'C', 0),
(274, 49, 3, 'B', 0),
(275, 49, 4, 'F', 1),
(276, 49, 5, 'C', 0),
(277, 49, 6, 'A', 0),
(278, 50, 1, 'A++', 0),
(279, 50, 2, 'F', 1),
(280, 50, 3, 'A', 0),
(281, 50, 4, 'F', 1),
(282, 50, 5, 'C', 0),
(283, 50, 6, 'F', 1),
(284, 51, 1, 'A+', 0),
(285, 51, 2, 'A+', 0),
(286, 51, 3, 'D', 0),
(287, 51, 4, 'F', 1),
(288, 51, 5, 'A++', 0),
(289, 51, 6, 'F', 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_bin NOT NULL,
  `surname` varchar(30) COLLATE utf8_bin NOT NULL,
  `email` varchar(50) COLLATE utf8_bin NOT NULL,
  `address` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(256) COLLATE utf8_bin NOT NULL,
  `userTypeId` int(11) NOT NULL,
  `username` varchar(60) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `address`, `password`, `userTypeId`, `username`) VALUES
(4, 'Fox', 'Ann', 'scarney@gmail.com', '526 Valenzuela Junction Suite 236 North Jason, IL ', 'fa2bf70e36667c52a85ffce13c1f4aa7c50c382172f638a53c62bebc0043c617', 1, 'ann.fox'),
(5, 'Walker', 'Amanda', 'djones@yahoo.com', '407 Aaron Lane Wilsontown, TN 71161', '46dea9f51b372131ed5d7fb08973c09fbfd9d4ea608510ff00dee13111cf6e28', 1, 'amanda.walker'),
(6, 'Villegas', 'Robert', 'antoniobaird@gmail.com', '38033 Parrish Forest Riosstad, NC 81878', '6d438e2c78b1a90b5aa6b148c0c85548a50ce860127f48fe2274ff61fb729f8f', 1, 'robert.villegas'),
(7, 'Rogers', 'Elizabeth', 'hhernandez@yahoo.com', '482 Wood Hill Apt. 229 Dunnhaven, TX 39569', '013831bf7c243ca4ca61e2e06eda3beca9e184775985dfb22ee68ab8e8063a3f', 1, 'elizabeth.rogers'),
(8, 'White', 'Tammy', 'brandon01@gmail.com', '160 Jill Circles Suite 351 Port Sarahberg, CT 5119', 'd7c264cde180bdb30f65cca2ffa3d582085d0cd38ab5087473d8d2e7f41c2401', 1, 'tammy.white'),
(9, 'Short', 'John', 'johnsonmelanie@hubbard-vargas.com', '54576 Potter Stream Catherinestad, KY 83348', '20204f21b5c236bd8af817bbd83696af90715ff4421bd569e6c4d91a81ef3bd9', 1, 'john.short'),
(10, 'Wilson', 'Elizabeth', 'gregoryhoffman@yahoo.com', '7472 Reynolds Road Danielleshire, MA 77112', '4674e078a88482e20c8d3ca0e3ed8e9eac623bddfb48d32e8bd7759b5e58f426', 1, 'elizabeth.wilson'),
(11, 'Nelson', 'Christopher', 'victoriaschwartz@joyce-jones.com', '456 Kelley Island Suite 938 New Danielton, IA 8190', '0a85620d88d1fd5d29f6d28a3f013bb5b1c1b42984704564a6090b954057301c', 1, 'christopher.nelson'),
(12, 'Houston', 'Robert', 'misty24@harris.com', '38034 Gordon Streets New Jasmine, UT 55841', '05fecbecc758b8018993cb63872512d0cf04828b31ef28de123553d3569c3592', 1, 'robert.houston'),
(13, 'Davies', 'Diana', 'tina59@delgado.com', '887 Barber Gardens Suite 281 Amyshire, PA 72386', '68de4ef1a91ae2b92820a0222ab060146150370b66786177ce90c8330ad1b348', 1, 'diana.davies'),
(14, 'Rhodes', 'Katherine', 'griffinjoshua@gmail.com', 'PSC 1218, Box 9558 APO AP 72573', 'f099c263b9a1e43ef178dcb2bf67aa981dff0ab46818aaa7e46c5854317b49e9', 1, 'katherine.rhodes'),
(15, 'Rodriguez', 'Michael', 'ijackson@greene.com', '1434 Cuevas Motorway Baldwinview, NH 02130', '2124b238836d9f469813cd1e585155730e108196603e4a6a13edade07e81d491', 1, 'michael.rodriguez'),
(16, 'Torres', 'Lori', 'gonzalezdonna@scott.com', '12800 Cynthia Keys Suite 952 Howardborough, ND 992', 'b2900f218292a153badafbc20251e165322b6d5823657618cda7ffc00f068ad2', 1, 'lori.torres'),
(17, 'Fields', 'Robert', 'amanda30@gmail.com', '54754 Scott Mill Suite 440 North Ginaton, OK 99416', '8a066eeb2a043ae2685b241619761d5fb5660e79184fcc27e239e40767accd4f', 1, 'robert.fields'),
(18, 'Bush', 'Tony', 'feliciarodriguez@yahoo.com', '800 Small Summit Apt. 422 Port Andrewside, NV 5453', '434e7c281b537ebacd2c885b2343b34ca017883991e8e0aa0ed1c4f42b295910', 1, 'tony.bush'),
(19, 'Austin', 'Brooke', 'alyssa26@yahoo.com', '2817 Pollard Stream Port Joseph, NY 17544', 'db6ad337b536b3859320a2fe679130f64dab4c7363aea17e59711b8274911e09', 1, 'brooke.austin'),
(20, 'Wilson', 'Kevin', 'james73@fuentes-robinson.com', '35173 Leonard Rue Alisonberg, MS 24792', '3ba5d590500c07f618d2d8d3f5ad20cd296cc1fefc735692ead07f3d682e373c', 1, 'kevin.wilson'),
(21, 'Jackson', 'Samantha', 'othompson@hotmail.com', '7663 Andrea Oval Johnstad, MS 33843', '00b0e61b44d0966828b03ca0bc36e52a0256020934bc4ae066979551502eac53', 1, 'samantha.jackson'),
(22, 'Patrick', 'Alexis', 'coxmark@gmail.com', '469 Harris Fork Bradyburgh, FL 60655', '24928620f6ca3b6030fde194d4b4549744015721dfa3cc6aca8db9d58485404b', 1, 'alexis.patrick'),
(23, 'Howell', 'Gregory', 'elliottkatherine@yahoo.com', '700 Frey Mount Apt. 395 Lake Steven, IN 39486', '5f3bd68a5123ab7f5176ca77c8e096204292a808acd14c57331a92db8382a91a', 1, 'gregory.howell'),
(24, 'Mendoza', 'Tamara', 'knewman@yahoo.com', '681 Sara Grove Suite 324 Hernandezton, WI 31076', 'bc906bf3b3b8ee65c0a0466b3646a9545e54e1879a410cf25a1a6d947efb98a8', 1, 'tamara.mendoza'),
(25, 'Lane', 'Tammy', 'sarah64@adkins-wolfe.com', '3136 Jordan Stream East Crystal, MN 96632', '2c92a412ecafac361c1f16c25a0aac146560f78d53de6210bf985a6b88c39949', 1, 'tammy.lane'),
(26, 'Watkins', 'Kathleen', 'rojasamanda@yahoo.com', '6331 Cathy Pass Allisonport, NY 55916', '585b749dc96244c06fd3e53619c02b0de68481139aeebe3d6f24288176bdbad6', 1, 'kathleen.watkins'),
(27, 'Hernandez', 'Derek', 'bfisher@hernandez.com', '8957 Lisa River Suite 142 Mauriceport, VT 71007', 'c7508d1e8ec20ff2ca531253d0f9cdc721f02c1563463f45e237428465233087', 1, 'derek.hernandez'),
(28, 'Clarke', 'Vanessa', 'dreed@gmail.com', '045 Guerra Loop Suite 576 Jenniferton, NM 56208', '3d1f8e342fad27e19ec66018432513e8cfcd24816aaf69e1e11ccc39da911e2f', 1, 'vanessa.clarke'),
(29, 'Fisher', 'Michael', 'ewilkerson@morton.com', '01916 Ray Rue Heatherside, SD 94686', 'ca5346dc1762dfe1b810686b93ab13bc1507284d6c530f41dc1c96895779a714', 1, 'michael.fisher'),
(30, 'Mann', 'Jennifer', 'murphylouis@hotmail.com', 'Unit 6838 Box 9035 DPO AA 06309', '98c47e51ed084fdbb7a3c11e3de19b755c50f163378fd7a1155c088db2ca9289', 1, 'jennifer.mann'),
(31, 'Chan', 'Jennifer', 'nicholas64@rivera.com', '6116 Patricia Plains Apt. 712 Kaufmanside, IA 8393', 'e0d7d203bfd19f1549fa9e8842d65f2f5b5188546fe47a335c3b757298acc496', 1, 'jennifer.chan'),
(32, 'Walters', 'Nancy', 'vcastillo@gmail.com', '599 Scott Stravenue Apt. 978 Lake Jeffreyport, VA ', '919fb9ecca41d4fa97eed57d5be52248729b4856aac9721d1f5e8b14a8b1f769', 1, 'nancy.walters'),
(33, 'Perez', 'Mark', 'cwhitaker@yahoo.com', '51728 Walter Mountain Suite 350 East Robert, KS 12', '4e62fac8359afd1d68266b9d95e9a3ebbbe8a0914160a0531bf2afbc62f2d3e2', 1, 'mark.perez'),
(34, 'Reeves', 'Julie', 'pattersonteresa@booker.com', '4807 Berry Vista Patrickton, VT 60455', 'c44a7280d58635865d42838a3f2bd38c1a6339fdb742ce1de543b835e1ca3246', 1, 'julie.reeves'),
(35, 'Hays', 'Alexander', 'sgonzalez@mendoza-thornton.com', '7629 Katrina Tunnel Joshualand, GA 86996', '5d7b6237b51d64632c40e40291f20709de4cac787551ab563864093dfd04fc8e', 1, 'alexander.hays'),
(36, 'Mcdaniel', 'Michelle', 'millsrachel@hotmail.com', '5441 Williams Causeway Lake Christian, ND 00542', 'ae9c2df39b75fb6c0d258d0546b339e7a691fa6cd2ddb272fd408213182615ef', 1, 'michelle.mcdaniel'),
(37, 'Pitts', 'Amy', 'briancarey@peterson-summers.com', '9631 Anderson Neck Apt. 531 East Andrew, NY 39844', '4aa83f053cb3f4596143cab3f7e7a15c6da7f4e382cd641b8f998a2417b835fc', 1, 'amy.pitts'),
(38, 'Scott', 'Lisa', 'kelsey55@gmail.com', 'PSC 6471, Box 8974 APO AE 24685', 'a4c6a2ef9c48a24e1805b6bbf66090c235d3ba453e6a802df0b282df4f8b4ceb', 1, 'lisa.scott'),
(39, 'Salazar', 'Tanner', 'heather63@walters.biz', '0804 Aaron Garden Suite 515 Lake Paula, HI 57851', '45e45c88e71ddda7de0cd2a438f82f9b9ee5e847f45a1d6ea34a73bf9b9ba7c3', 1, 'tanner.salazar'),
(40, 'Wright', 'Dana', 'clarklisa@gmail.com', '682 Monica Mall Apt. 879 Victoriatown, SD 79904', 'f25bffb2301877c88dcb83237fe397d68e2d5ea2e697bb369d57ab020983d372', 1, 'dana.wright'),
(41, 'Thompson', 'Jeffrey', 'dwhite@preston.net', '733 Stephens Knoll Wrightport, MS 07924', 'b9a547bc1ceefc8f0a0d5e63f6486aaa370e53d0f3238b1de0a71148d2801c94', 1, 'jeffrey.thompson'),
(42, 'Richardson', 'Ryan', 'john84@elliott-rodriguez.info', '0414 Susan Street Suite 086 South Michelemouth, MN', '104e821892b9dc0758b9e706e9540456e11a77b5d7bbee433ba27540555a2787', 1, 'ryan.richardson'),
(43, 'Dudley', 'Dawn', 'tonyamiddleton@yahoo.com', '46599 Jessica Center West Elizabethville, OK 79557', '12cf29b1089183f6edcd43fd62118a2028ab883b06b96deca4a0ec4343bc6df8', 1, 'dawn.dudley'),
(44, 'Fischer', 'Leah', 'briannguyen@king.com', '739 Dalton Crossing Royburgh, OR 88840', 'e042c9cd70f426e0fadb645abb35227abe72a052986389d440850b294d27404f', 1, 'leah.fischer'),
(45, 'Wilson', 'Jose', 'ryanwarren@evans.net', '481 Randy Canyon Flowersstad, NJ 52176', 'aa40ea304471c7d87d29619a06b603dd2b86835088a58284bb7dfe4807421406', 1, 'jose.wilson'),
(46, 'Rice', 'Scott', 'shelley02@lewis.com', '806 Steven Freeway North Emilymouth, MT 23056', 'adad825b5dcfebe380d1a440b28053806f2efaab6b85cf8dedba259a55d16c80', 1, 'scott.rice'),
(47, 'Simmons', 'Anthony', 'gonzalestom@elliott.com', '970 Ashley Lakes Port Sheri, KS 93741', 'c8431d0aa24a953b30b8db66b07fd6a680c3d1599d96841971f5609a56aa1e19', 1, 'anthony.simmons'),
(48, 'Horn', 'Carolyn', 'jacobslarry@cantu-clark.com', '681 Michele Plaza Suite 680 Huntfurt, AL 69451', '0f9bde3fe97ad94893b291d02dc8b6a5bd9eb4b83e3b18999dcf4a3ff7e44d9b', 1, 'carolyn.horn'),
(49, 'Wells', 'Nicole', 'haleyryan@yahoo.com', '72189 Arnold Lake West Kathleenton, ME 08139', '180e18f25beb0a5e72228dc4bd59345382baf04d88b6d73d0b72ad44065e2b86', 1, 'nicole.wells'),
(50, 'Gates', 'Jorge', 'pricekevin@gmail.com', '51486 Archer Point Apt. 497 Robertport, GA 21365', '07c2420a98e251a2d670210b680e269435e52f933dd096218d5629cba2eb374c', 1, 'jorge.gates'),
(51, 'Price', 'Micheal', 'jeremylane@hotmail.com', '698 Michele Crescent Apt. 989 South Scottstad, AZ ', 'b8e9e0ee7ac7f8b5095210a282db6491de244e2a8f6f8caa0b62fbe1943234e3', 1, 'micheal.price'),
(52, 'Roy', 'Samantha', 'osmith@torres.com', '3513 Briggs Village Suite 861 Port Andrewstad, MA ', '9a7db5e03fc67a5e7ce174d386f3d88ebb61ad16df3d93a8d9d38eec04881428', 1, 'samantha.roy'),
(53, 'VLASAK', 'Joel', 'test@gmail.com', '6 route de chavagne', '1057a9604e04b274da5a4de0c8f4b4868d9b230989f8c8c6a28221143cc5a755', 2, 'teacher'),
(54, 'Admin', 'Admin', 'Admin@gmail.com', '6 route de la dictaturzzz', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 3, 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `users_types`
--

CREATE TABLE `users_types` (
  `id` int(11) NOT NULL,
  `type` varchar(40) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `users_types`
--

INSERT INTO `users_types` (`id`, `type`) VALUES
(3, 'Administration'),
(1, 'Student'),
(2, 'Teacher');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id`),
  ADD KEY `moduleId` (`moduleId`),
  ADD KEY `typeId` (`typeId`);

--
-- Index pour la table `components_types`
--
ALTER TABLE `components_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_component_type` (`type`);

--
-- Index pour la table `enrolments`
--
ALTER TABLE `enrolments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_enrolment` (`userId`,`moduleId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `moduleId` (`moduleId`);

--
-- Index pour la table `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_marks` (`userId`,`componentId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `componentId` (`componentId`);

--
-- Index pour la table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_module` (`name`);

--
-- Index pour la table `modules_results`
--
ALTER TABLE `modules_results`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_module_result` (`userId`,`moduleId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `moduleId` (`moduleId`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_login` (`name`,`surname`,`username`),
  ADD KEY `userTypeId` (`userTypeId`);

--
-- Index pour la table `users_types`
--
ALTER TABLE `users_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type` (`type`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `components`
--
ALTER TABLE `components`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `components_types`
--
ALTER TABLE `components_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `enrolments`
--
ALTER TABLE `enrolments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT pour la table `marks`
--
ALTER TABLE `marks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=581;

--
-- AUTO_INCREMENT pour la table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `modules_results`
--
ALTER TABLE `modules_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT pour la table `users_types`
--
ALTER TABLE `users_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `components`
--
ALTER TABLE `components`
  ADD CONSTRAINT `components_ibfk_1` FOREIGN KEY (`moduleId`) REFERENCES `modules` (`id`),
  ADD CONSTRAINT `components_ibfk_2` FOREIGN KEY (`typeId`) REFERENCES `components_types` (`id`);

--
-- Contraintes pour la table `enrolments`
--
ALTER TABLE `enrolments`
  ADD CONSTRAINT `enrolments_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `enrolments_ibfk_2` FOREIGN KEY (`moduleId`) REFERENCES `modules` (`id`);

--
-- Contraintes pour la table `marks`
--
ALTER TABLE `marks`
  ADD CONSTRAINT `marks_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `marks_ibfk_2` FOREIGN KEY (`componentId`) REFERENCES `components` (`id`);

--
-- Contraintes pour la table `modules_results`
--
ALTER TABLE `modules_results`
  ADD CONSTRAINT `modules_results_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `modules_results_ibfk_2` FOREIGN KEY (`moduleId`) REFERENCES `modules` (`id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`userTypeId`) REFERENCES `users_types` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
